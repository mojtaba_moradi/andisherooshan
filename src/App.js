import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import PanelAdmin from "./PanelAdmin";
import { ToastContainer, toast } from "react-toastify";
import WebSite from "./Website";
import checkAuth from "./PanelAdmin/util/chechkAuth";
import routes from "./PanelAdmin/value/routes";
import LoginScreen from "./PanelAdmin/screen/LoginScreen";
import AtomLoading from "./PanelAdmin/util/Loadings/AtomLoading";

const App = () => {
  return (
    <div className="App">
      <Switch>
        <Route
          path="/panelAdmin"
          render={() => {
            return checkAuth() ? <PanelAdmin /> : <LoginScreen />;
          }}
        />
        <Route path="/" component={WebSite} />
      </Switch>
      <ToastContainer rtl />
    </div>
  );
};

export default App;
