import React, { Fragment } from "react";
import Header from "../../components/Header";
import "./index.scss";
import WebString from "../../value/WebString";
import Footer from "../../components/Footer";
const AboutUs = () => {
  return (
    <Fragment>
      <div className="us-container">
        <Header />
        <main className="main-wrapper">
          <div className="web-type-wrapper fade-in-up">
            <div class="us-wrapper">
              <h1>درباره ما</h1>
              <p>{WebString.string.ABOUT_US}</p>
            </div>
          </div>
        </main>
      </div>
      <Footer />
    </Fragment>
  );
};

export default AboutUs;
