import React from "react";
import { Switch, Route } from "react-router-dom";
import HomeScreen from "./screen/HomeScreen";
import WebSiteMain from "./components/WebSiteMain";
import Footer from "./components/Footer";

const WebSite = () => {
  return (
    <React.Fragment>
      <WebSiteMain />
    </React.Fragment>
  );
};

export default WebSite;
