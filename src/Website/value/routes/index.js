const GS_WEB_HOME_SCREEN = "/";
const GS_WEB_ABOUT_US = "/aboutUs";
const GS_WEB_CONTACT_US = "/contactUs";
const GS_WEB_PAYMENT_US = "/paymentVerification";

const routes = {
  GS_WEB_HOME_SCREEN,
  GS_WEB_ABOUT_US,
  GS_WEB_CONTACT_US,
  GS_WEB_PAYMENT_US,
};

export default routes;
