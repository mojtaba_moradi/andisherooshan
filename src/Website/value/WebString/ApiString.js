const BASE_URL = "https://andisheroshan.com/api/v1";
const COURSE = "/admin/course";
const VOICE_UPLOAD = "/admin/voiceUpload";
const VIDEO_UPLOAD = "/admin/videoUpload";
const UPLOAD = "/admin/upload";
const SECTION = "/admin/section";
const CATEGORY = "/admin/category";
const BLOG = "/admin/blog";
const SLIDER = "/admin/slider";
const COURSE_SEARCH = "/admin/course/s";
const BLOG_SEARCH = "/admin/blog/s";
const LOGIN = "/admin/login";

const ApiString = {
  COURSE,
  VOICE_UPLOAD,
  VIDEO_UPLOAD,
  UPLOAD,
  SECTION,
  CATEGORY,
  BLOG,
  SLIDER,
  COURSE_SEARCH,
  BLOG_SEARCH,
  LOGIN
};
export default ApiString;
