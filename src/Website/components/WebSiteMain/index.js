import React from "react";
import "./index.scss";
import { Switch, Route, Redirect } from "react-router-dom";
import ProtectedRoute from "../../util/ProtectedRoute";
import formatRoute from "../../util/formatRoute";

const WebSiteMain = () => {
  return (
    <div className="WebSite-main-container">
      <Switch>
        {formatRoute.map(route => {
          return <ProtectedRoute path={route.path} component={route.component} exact={route.exact} />;
        })}
      </Switch>
    </div>
  );
};

export default WebSiteMain;
