import React, { useState } from "react";
import "./index.scss";
import { Link } from "react-router-dom";
import HamburgerMenu from "../HamburgerMenu";
import MobileApp from "../MobileApp";
import btnBg from "../../assets/images/img/bg-btn.png";
import logo from "../../assets/images/img/logo.png";

import SliderScrollFull from "../SliderScrollFull";
const Header = props => {
  return (
    <header className="header-container landingBgColor">
      <div className="header-box-sizing web-container">
        <div className="header-top-wrapper">
          <div className="web-logo">
            <Link to="/">
              <img src={logo} alt="andishe roshan" />
            </Link>
          </div>
          <nav className="nav-container">
            <HamburgerMenu />
          </nav>
        </div>

        {props.main && (
          <div className="header-bottom-wrapper">
            <div className="header-description-wrapper  ">
              <h1 className="bold fontUpperAouto47 fade-in-left">
                <span>{"اندیشه روشن"}</span>

                <span>{" آموزشگاهی در جیب شما"}</span>
              </h1>
              <h3 className={"fontUpperAouto24 fade-in-left"}>
                <span>{"با نصب ، اپلیکشن آموزشی اندیشه روشن هر روز به هدفت نزدیکتر میشوی"}</span>
              </h3>
              <div className="btn-wrapper transition0-3 fade-in-up ">
                <a className=" btn-download transition0-2 btn-ios" href="#"></a>
                <a className=" btn-download transition0-2 btn-android " href="#"></a>
                <a className=" btn-download transition0-2 btn-chanel" target="_blank" href="#"></a>
              </div>
            </div>
            <MobileApp>
              <SliderScrollFull />
            </MobileApp>
          </div>
        )}
      </div>
    </header>
  );
};

export default Header;
