import React from "react";
import "./index.scss";
import { Link } from "react-router-dom";
import routes from "../../value/routes";
const Footer = () => {
  return (
    <div className="footer-wrapper landingBgColor">
      <div className="web-container ">
        <div className="footer-wrapper-top">{" اندیشه روشن ، آینده روشن "}</div>
        <div className="footer-wrapper-content">
          <div className="footer-content footer-unit sendNews-email">
            <div className="news-title">آخـرین اخـبار و اتفـاقات ویژه در ایمـیل شما</div>
            <div className="input-label-box">
              <input type="text" placeholder="آدرس ایمیل خود را وارد نمائید" />
              <span className="btn-container">
                <button className=" btns-primary" type="button">
                  عضویت
                </button>
              </span>
            </div>
            <div className="news-title-caption">فقط پیشنهادها و خبرهای ویژه را برای شما ارسال می کنیم</div>
          </div>
          <div className="footer-content footer-unit about">
            <div>
              <span className="about-subtitle">ارتباط بیشتر</span>
              <ul>
                <li>
                  <Link>حریم خصوصی</Link>{" "}
                </li>
                <li>
                  <Link to={routes.GS_WEB_CONTACT_US}>تماس با ما</Link>{" "}
                </li>
                <li>
                  <Link>قوانین و مقررات</Link>{" "}
                </li>
              </ul>
            </div>
            <div>
              <span className="about-subtitle">بیشتر بدانید</span>
              <ul>
                <li>
                  <Link>درباره اندیشه روشن </Link>
                </li>
                <li>
                  <Link>سوالات متداول </Link>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <footer id="copyright" className="copyright-wrapper">
        <div className="sl-direction container">
          <div className="copyright-text">تمامی حقوق برای اندیشه روشن محفوظ است.</div>
        </div>
      </footer>
    </div>
  );
};

export default Footer;
