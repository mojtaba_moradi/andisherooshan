import React from "react";
import "./index.scss";
import { Link } from "react-router-dom";
import routes from "../../../value/routes";
const MenuItem = props => {
  const { open } = props;
  const menuitemTitle = [
    { title: "تماس با ما", href: routes.GS_WEB_CONTACT_US },
    { title: "درباره ما", href: routes.GS_WEB_ABOUT_US }
  ];
  const style = {
    container: {
      whiteSpace: "nowrap",
      transform: open ? "translateX(0)" : "translateX(-250px)",
      transition: " all ease-out 200ms, opacity "
    }
  };
  const manuTitle_map = menuitemTitle.map((menu, index) => {
    return (
      <div key={index}>
        <Link to={menu.href}>{menu.title}</Link>
      </div>
    );
  });
  return (
    <div style={{ overflow: "hidden" }}>
      <div className="transition0-3" style={{ ...style.container }} className="menuTitle-box centerAll">
        {manuTitle_map}
      </div>
    </div>
  );
};

export default MenuItem;
