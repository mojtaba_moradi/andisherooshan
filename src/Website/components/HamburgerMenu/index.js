import React, { useState, useEffect } from "react";

import MenuButton from "./MenuButton";
import MenuItem from "./MenuItem";

const HamburgerMenu = props => {
  const [state, setState] = useState({ menuOpen: false });

  const handleMenuClick = () => {
    setState({ ...state, menuOpen: !state.menuOpen });
  };
  const style = {
    container: { display: "flex", flexFlow: "row-reverse", overflow: "hidden" }
  };
  return (
    <div className="centerAll" style={{ ...style.container }}>
      <MenuButton open={state.menuOpen} onClick={handleMenuClick} color="white" />
      <MenuItem open={state.menuOpen} />
    </div>
  );
};

export default HamburgerMenu;
