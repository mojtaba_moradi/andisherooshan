import sectionOne from "../../assets/images/img/home.png";
import sectionTwo from "../../assets/images/img/navigation.png";
import sectionTree from "../../assets/images/img/page.png";
import sectionfour from "../../assets/images/img/player.png";
import sectionfive from "../../assets/images/img/page.png";
import sectionsix from "../../assets/images/img/file.png";
import WebString from "../../value/WebString";
const mainSections = [
  {
    direction: "rtl",
    subtitle: WebString.string.SECTION_ONE_SUBTITLE,
    color: "#509646",
    description: WebString.string.SECTION_ONE_DESCRIPTION,
    image: sectionOne
  },
  {
    direction: "ltr",
    subtitle: WebString.string.SECTION_TWO_SUBTITLE,
    color: "#413f8f",
    description: WebString.string.SECTION_TWO_DESCRIPTION,
    image: sectionTwo
  },
  {
    direction: "rtr",
    subtitle: WebString.string.SECTION_TREE_SUBTITLE,
    color: "orange",
    description: WebString.string.SECTION_TREE_DESCRIPTION,
    image: sectionTree
  },
  {
    direction: "ltr",
    subtitle: WebString.string.SECTION_FOUR_SUBTITLE,
    color: "orange",
    description: WebString.string.SECTION_FOUR_DESCRIPTION,
    image: sectionfour
  },
  {
    direction: "rtl",
    subtitle: WebString.string.SECTION_FIVE_SUBTITLE,
    color: "#509646",
    description: WebString.string.SECTION_FIVE_DESCRIPTION,
    image: sectionfive
  },
  {
    direction: "ltr",
    subtitle: WebString.string.SECTION_SIX_SUBTITLE,
    color: "#413f8f",
    description: WebString.string.SECTION_SIX_DESCRIPTION,
    image: sectionsix
  }
];
export default mainSections;
