import HomeScreen from "../screen/HomeScreen";
import routes from "../value/routes";
import AboutUs from "../screen/AboutUs";
import ContactUS from "../screen/ContactUS";
import SubmitPayment from "../screen/SubmitPayment";

const formatRoute = [
  { path: routes.GS_WEB_ABOUT_US, component: AboutUs },
  { path: routes.GS_WEB_CONTACT_US, component: ContactUS },
  { path: routes.GS_WEB_PAYMENT_US, component: SubmitPayment },
  { path: routes.GS_WEB_HOME_SCREEN, component: HomeScreen },
];
export default formatRoute;
