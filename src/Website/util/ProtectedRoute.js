import React from "react";
import { Redirect, Route } from "react-router-dom";

const ProtectedRoute = ({ component: Component, ...rest }) => {
  console.log({ ...rest });
  return (
    <Route
      {...rest}
      render={props => {
        console.log({ props });

        return <Component {...props} />;
      }}
    />
  );
};

export default ProtectedRoute;
