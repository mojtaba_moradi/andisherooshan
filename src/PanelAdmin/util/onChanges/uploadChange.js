import { post } from "../../api";
import toastify from "../toastify";

const uploadChange = async (props) => {
  const { event, setLoading, fileType, setState, valid } = props;
  console.log("vared ersal file uploadi shodid mishavad");

  let returnData = false;
  let files = event.files[0];
  if (files)
    switch (valid) {
      case "image":
        if (files.type.includes("image")) returnData = await post.imageUpload(files, setLoading, fileType, setState);
        break;
      case "video":
        if (files.type.includes("video")) returnData = await post.videoUpload(files, setLoading, fileType, setState);
      case "voice":
        if (files.type.includes("voice") || files.type.includes("audio")) returnData = await post.voiceUpload(files, setLoading, fileType, setState);
      default:
        break;
    }
  if (!returnData && files) toastify("فایل شما نباید " + files.type + " باشد", "error");
  setState((prev) => ({ ...prev, progressPercentImage: null, progressPercentPlay: null }));
  return returnData;
};
export default uploadChange;
