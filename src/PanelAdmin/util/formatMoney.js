const formatMoney = (number) => {
  // console.log({ number });

  return number && number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
};

export default formatMoney;
