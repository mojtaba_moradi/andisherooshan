import React from "react";
import "./index.scss";
const LoadingDotFun = () => {
  return (
    <div class="loading">
      <div class="dot"></div>
      <div class="dot"></div>
      <div class="dot"></div>
      <div class="dot"></div>
      <div class="dot"></div>
      <div class="dot"></div>
    </div>
  );
};

export default LoadingDotFun;
