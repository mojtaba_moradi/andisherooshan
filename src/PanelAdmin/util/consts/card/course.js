import React from "react";
import PanelString from "../../../value/PanelString";

const course = (data) => {
  const cardFormat = [];

  for (let index in data) {
    let dataIndex = data[index];
    let description = dataIndex.description ? dataIndex.description : "";
    let price = dataIndex.price ? dataIndex.price : "";
    let title = dataIndex.title ? dataIndex.title : "";
    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      image: { value: data[index].cover },
      body: [
        {
          right: [{ elementType: "text", value: title, style: { color: "black", fontSize: "1.3em", fontWeight: "bold" } }],
        },
        { right: [{ elementType: "text", value: description, title: description, style: { color: PanelString.color.GRAY, fontSize: "0.9em", fontWeight: "500" } }] },

        {
          left: [{ elementType: "price", value: price, direction: "ltr" }],
        },
      ],
    });
  }
  return cardFormat;
};

export default course;
