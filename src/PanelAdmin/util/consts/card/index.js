import course from "./course";
import user from "./user";
import admin from "./admin";
import club from "./club";
import blog from "./blog";
import seminar from "./seminar";
import audioBook from "./audioBook";
import introductionJob from "./introductionJob";
import soldCourse from "./soldCourse";
import soldAudioBook from "./soldAudioBook";
import soldSeminar from "./soldSeminar";
// import introductionJob from "./introductionJob";

const card = {
  course,
  club,
  blog,
  user,
  seminar,
  audioBook,
  introductionJob,
  admin,
  soldCourse,
  soldAudioBook,
  soldSeminar,
};
export default card;
