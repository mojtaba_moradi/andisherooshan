import React from "react";

const seenAudioBook = (data) => {
  console.log({ tableaudi: data });

  const thead = ["#", "کتاب صوتی", "قسمت"];
  let tbody = [];
  for (let index = 0; index < data.length; index++) {
    console.log({ audiomap: data[index] });

    let NotEntered = "وارد نشده";
    let audioBook = data[index].audioBook ? data[index].audioBook : NotEntered;
    let audioBookTitle = audioBook.title ? audioBook.title : NotEntered;
    let section = data[index].section ? data[index].section : NotEntered;
    let sectionTitle = section.title ? section.title : NotEntered;

    tbody.push({
      data: [audioBookTitle, sectionTitle],
    });
  }
  return [thead, tbody];
};

export default seenAudioBook;
