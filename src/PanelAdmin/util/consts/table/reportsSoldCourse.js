import React from "react";
import formatMoney from "../../formatMoney";

const reportsSoldCourse = (data) => {
  // , "دوره", "مشتری", "تاریخ", "قیمت", "ناشر دوره"
  const thead = ["#", "نام دوره", "نام مشتری", "ناشر ", "شماره همراه مشتری ", "تاریخ", "قیمت"];
  let tbody = [];
  if (data)
    for (let index = 0; index < data.length; index++) {
      let NotEntered = "وارد نشده";
      let dateSplit = data[index].date ? data[index].date.split(" ") : "";
      let date = dateSplit ? <span>{dateSplit[1] + " - " + dateSplit[0]}</span> : NotEntered;
      let course = data[index].course ? data[index].course : NotEntered;
      let courseTitle = course.title ? course.title : NotEntered;
      let publisher = data[index].publisher ? data[index].publisher : NotEntered;
      let publisherName = publisher.name ? publisher.name : NotEntered;
      let customer = data[index].customer ? data[index].customer : NotEntered;
      let customerName = customer.fullName ? customer.fullName : NotEntered;
      let customerPhoneNumber = customer.phoneNumber ? customer.phoneNumber : NotEntered;

      let price = data[index].price ? formatMoney(data[index].price) : "0";

      tbody.push({
        data: [courseTitle, customerName, publisherName, customerPhoneNumber, date, price],
      });
    }
  return [thead, tbody];
};

export default reportsSoldCourse;
