import React from "react";
import formatMoney from "../../formatMoney";

const reportsSoldAudioBookPublisher = (data) => {
  const thead = ["#", "کتاب صوتی", "مشتری", "تاریخ", "ناشر کتاب صوتی"];
  let tbody = [];
  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let dateSplit = data[index].date ? data[index].date.split(" ") : "";
    let date = dateSplit ? <span>{dateSplit[1] + " - " + dateSplit[0]}</span> : NotEntered;
    let audioBook = data[index].audioBook ? data[index].audioBook : NotEntered;
    let audioBookTitle = audioBook.title ? audioBook.title : NotEntered;
    let publisher = data[index].publisher ? data[index].publisher : NotEntered;
    let publisherName = publisher.name ? publisher.name : NotEntered;
    let customer = data[index].customer ? data[index].customer : NotEntered;
    let customerName = customer.fullName ? customer.fullName : NotEntered;

    tbody.push({
      data: [audioBookTitle, customerName, date, publisherName],
    });
  }
  return [thead, tbody];
};

export default reportsSoldAudioBookPublisher;
