import showScenario from "./ShowScenario";
import ShowScenarioDataInModal from "./ShowScenarioDataInModal";
import ShowScenarioDataInModalTwo from "./ShowScenarioDataInModalTwo";
import reportsSoldSeminar from "./reportsSoldSeminar";
import reportsSoldSeminarPublisher from "./reportsSoldSeminarPublisher";
import reportsSoldAudioBook from "./reportsSoldAudioBook";
import reportsSoldAudioBookPublisher from "./reportsSoldAudioBookPublisher";
import reportsSoldCourse from "./reportsSoldCourse";
import reportsSoldCoursePublisher from "./reportsSoldCoursePublisher";
import seenSeminar from "./seenSeminar";
import seenCourse from "./seenCourse";
import seenAudioBook from "./seenAudioBook";
const table = {
  reportsSoldAudioBook,
  reportsSoldSeminar,
  showScenario,
  ShowScenarioDataInModal,
  ShowScenarioDataInModalTwo,
  reportsSoldCourse,
  seenSeminar,
  seenCourse,
  seenAudioBook,
  reportsSoldSeminarPublisher,
  reportsSoldAudioBookPublisher,
  reportsSoldCoursePublisher,
};
export default table;
