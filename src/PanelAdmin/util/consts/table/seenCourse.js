import React from "react";

const seenCourse = (data) => {
  console.log({ tablecourse: data });

  const thead = ["#", "دوره", "قسمت"];
  let tbody = [];
  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let course = data[index].course ? data[index].course : NotEntered;
    let courseTitle = course.title ? course.title : NotEntered;
    let section = data[index].section ? data[index].section : NotEntered;
    let sectionTitle = section.title ? section.title : NotEntered;

    tbody.push({
      data: [courseTitle, sectionTitle],
    });
  }
  return [thead, tbody];
};

export default seenCourse;
