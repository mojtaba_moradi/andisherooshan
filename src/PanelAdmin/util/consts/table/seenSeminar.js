import React from "react";
import formatMoney from "../../formatMoney";

const seenSeminar = (data) => {
  console.log({ tablesemina: data });

  const thead = ["#", "سمینار", "قسمت"];
  let tbody = [];
  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let seminar = data[index].seminar ? data[index].seminar : NotEntered;
    let seminarTitle = seminar.title ? seminar.title : NotEntered;
    let section = data[index].section ? data[index].section : NotEntered;
    let sectionTitle = section.title ? section.title : NotEntered;

    tbody.push({
      data: [seminarTitle, sectionTitle],
    });
  }
  return [thead, tbody];
};

export default seenSeminar;
