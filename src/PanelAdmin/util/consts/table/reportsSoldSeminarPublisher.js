import React from "react";
import formatMoney from "../../formatMoney";

const reportsSoldSeminarPublisher = (data) => {
  const thead = ["#", "سمینار", "مشتری", "تاریخ", "ناشر سمینار"];
  let tbody = [];
  if (data)
    for (let index = 0; index < data.length; index++) {
      let NotEntered = "وارد نشده";
      let dateSplit = data[index].date ? data[index].date.split(" ") : "";
      let date = dateSplit ? <span>{dateSplit[1] + " - " + dateSplit[0]}</span> : NotEntered;
      let seminar = data[index].seminar ? data[index].seminar : "";
      let seminarTitle = seminar ? seminar.title : NotEntered;
      let publisher = data[index].publisher ? data[index].publisher : NotEntered;
      let publisherName = publisher.name ? publisher.name : NotEntered;
      let customer = data[index].customer ? data[index].customer : NotEntered;
      let customerName = customer.fullName ? customer.fullName : NotEntered;

      tbody.push({
        data: [seminarTitle, customerName, date, publisherName],
      });
    }
  return [thead, tbody];
};

export default reportsSoldSeminarPublisher;
