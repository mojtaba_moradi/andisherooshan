import dictionary from "../../dictionary";
const slider = (data) => {
  const cardImageFormat = [];
  for (let index in data) {
    let courseOrBlog;
    // data[index].type === "BLOG" ? data[index].blog : data[index].type === "COURSE" ? data[index].course : "";
    if (data[index].type === "BLOG") courseOrBlog = courseOrBlog = data[index].blog;
    else if (data[index].type === "COURSE") courseOrBlog = data[index].course;
    else if (data[index].type === "SEMINAR") courseOrBlog = data[index].seminar;
    else if (data[index].type === "AUDIO_BOOK") courseOrBlog = data[index].audioBook;
    else if (data[index].type === "JOB") courseOrBlog = data[index].job;

    cardImageFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      image: { value: data[index].image },
      title: dictionary(data[index].type),
      description: courseOrBlog && courseOrBlog.title,
    });
  }
  return cardImageFormat;
};

export default slider;
