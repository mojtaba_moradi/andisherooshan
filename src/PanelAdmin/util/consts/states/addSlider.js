const addSlider = {
  Form: {
    type: {
      label: " ",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نوع والد",
      },
      childValue: [
        { name: "بلاگ", value: "BLOG" },
        { name: "دوره", value: "COURSE" },
        { name: "سمینار", value: "SEMINAR" },
        { name: "کتاب صوتی", value: "AUDIO_BOOK" },
        { name: "معرفی مشاغل", value: "JOB" },
      ],
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    course: {
      label: "دوره :",
      elementType: "inputSearch",
      kindOf: "",
      value: "",
      validation: {
        required: true,
      },
      valid: true,
      touched: false,
    },
    blog: {
      label: " بلاگ :",
      elementType: "inputSearch",
      kindOf: "",
      value: "",
      validation: {
        required: true,
      },
      valid: true,
      touched: false,
    },
    audioBook: {
      label: " کتاب صوتی :",
      elementType: "inputSearch",
      kindOf: "",
      value: "",
      validation: {
        required: true,
      },
      valid: true,
      touched: false,
    },
    seminar: {
      label: " سمینار :",
      elementType: "inputSearch",
      kindOf: "",
      value: "",
      validation: {
        required: true,
      },
      valid: true,
      touched: false,
    },
    job: {
      label: " معرفی مشاغل :",
      elementType: "inputSearch",
      kindOf: "",
      value: "",
      validation: {
        required: true,
      },
      valid: true,
      touched: false,
    },
    image: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
  },

  formIsValid: false,
};

export default addSlider;
