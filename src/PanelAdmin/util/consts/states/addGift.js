const addGift = {
  Form: {
    type: {
      label: " ",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نوع والد",
      },
      childValue: [
        { name: "دوره", value: "course" },
        { name: "سمینار", value: "seminar" },
        { name: "کتاب صوتی", value: "audioBook" },
      ],
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    course: {
      label: "دوره :",
      elementType: "inputSearch",
      kindOf: "",
      value: "",
      validation: {
        required: true,
      },
      valid: true,
      touched: false,
    },

    audioBook: {
      label: " کتاب صوتی :",
      elementType: "inputSearch",
      kindOf: "",
      value: "",
      validation: {
        required: true,
      },
      valid: true,
      touched: false,
    },
    seminar: {
      label: " سمینار :",
      elementType: "inputSearch",
      kindOf: "",
      value: "",
      validation: {
        required: true,
      },
      valid: true,
      touched: false,
    },
    user: {
      label: " کاربر :",
      elementType: "inputSearch",
      kindOf: "",
      value: "",
      validation: {
        required: true,
      },
      valid: true,
      touched: false,
    },
  },

  formIsValid: false,
};

export default addGift;
