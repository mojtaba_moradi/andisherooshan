const addAudioBook = {
  Form: {
    title: {
      label: "عنوان :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    description: {
      label: "توضیحات:",
      elementType: "textarea",
      elementConfig: {
        placeholder: "توضیحات",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    aboutAudioBook: {
      label: "درباره کتاب صوتی :",
      elementType: "textarea",
      elementConfig: {
        placeholder: "درباره کتاب صوتی",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    price: {
      label: "قیمت  :",
      elementType: "input",
      elementConfig: {
        type: "number",
        placeholder: "قیمت اصلی",
      },
      value: "",
      validation: {
        minLength: 1,
        isNumeric: true,
        required: true,
      },
      valid: false,
      touched: false,
    },
    publisher: {
      label: "ناشر :",

      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "ناشر",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    cover: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
  },

  formIsValid: false,
};

export default addAudioBook;
