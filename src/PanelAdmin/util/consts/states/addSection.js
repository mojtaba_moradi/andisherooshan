const addSection = {
  Form: {
    type: {
      label: " ",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نوع والد",
      },
      childValue: [
        { name: "صوتی", value: "VOICE" },
        { name: "تصویری", value: "VIDEO" },
      ],
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    title: {
      label: "عنوان :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    description: {
      label: "توضیحات:",
      elementType: "textarea",
      elementConfig: {
        placeholder: "توضیحات",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },

    video: {
      label: "لینک فایل تصویری :",
      elementType: "input",
      kindOf: "video",
      value: "",
      elementConfig: {
        type: "text",
        placeholder: "http://",
      },
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    voice: {
      label: "آپلود صوت :",
      elementType: "inputFile",
      kindOf: "voice",
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
  },

  formIsValid: false,
};

export default addSection;
