const addBlog = {
  Form: {
    type: {
      label: " ",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نوع والد",
      },
      childValue: [
        { name: "صوتی", value: "VOICE" },
        { name: "تصویری", value: "VIDEO" },
      ],
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    title: {
      label: "عنوان :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    description: {
      label: "توضیحات:",
      elementType: "textarea",
      elementConfig: {
        placeholder: "توضیحات",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },

    aboutBlog: {
      label: "درباره بلاگ :",
      elementType: "textarea",
      elementConfig: {
        placeholder: "درباره بلاگ",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    category: {
      label: "دسته بندی :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "دسته بندی",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    dataUrl: {
      label: "آپلود فایل :",
      elementType: "inputFile",
      kindOf: "",
      value: "",
      elementConfig: {
        type: "text",
        placeholder: "http://",
      },
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    cover: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
  },

  formIsValid: false,
};

export default addBlog;
