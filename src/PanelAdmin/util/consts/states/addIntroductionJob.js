const addIntroductionJob = {
  Form: {
    title: {
      label: "عنوان :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    description: {
      label: "توضیحات:",
      elementType: "textarea",
      elementConfig: {
        placeholder: "توضیحات"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    aboutJob: {
      label: "درباره شغل :",
      elementType: "textarea",
      elementConfig: {
        placeholder: "درباره شغل"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },

    cover: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },

  formIsValid: false
};

export default addIntroductionJob;
