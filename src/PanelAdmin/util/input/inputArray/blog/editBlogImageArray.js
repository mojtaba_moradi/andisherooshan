const constArray = {
  dataUrl: { label: "آپلود فایل", type: "file" }
};
const editBlogArray = [];
for (const key in constArray) {
  editBlogArray.push({
    name: key,
    value: constArray[key]
  });
}
export default editBlogArray;
