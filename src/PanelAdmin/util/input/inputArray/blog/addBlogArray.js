const constMind = {
  type: {
    label: "نوع قسمت",
    type: "button",

    text: [
      { title: "تصویری", value: "VIDEO" },
      { title: "صوتی", value: "VOICE" }
    ]
  },
  title: { label: "عنوان", type: "text" },
  description: { label: "توضیحات", type: "textarea" },
  aboutBlog: { label: " توضیحات بلاگ", type: "textarea" },
  dataUrl: { label: "آپلود فایل", type: "file" },
  cover: { label: "عکس بلاگ", type: "file" }
};

const addBlogArray = [];
for (const key in constMind) {
  addBlogArray.push({
    name: key,
    value: constMind[key]
  });
}
export default addBlogArray;
