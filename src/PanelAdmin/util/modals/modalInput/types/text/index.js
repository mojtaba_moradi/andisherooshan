import React from "react";
import "./index.css";


const InputText = ({onTextChange, inputType,inputValue}) => {

    return (
        <input type={inputType} onChange={onTextChange} value={inputValue}/>
    )
};

export default InputText;