import CourseScreen from "../screen/Courses/CourseScreen";
import CategoryScreen from "../screen/Categories/CategoryScreen";
import BlogsScreen from "../screen/Blogs/BlogsScreen";
import SliderScreen from "../screen/Sliders/SliderScreen";
import routes from "../value/routes";
import LoginScreen from "../screen/LoginScreen";
import DashboardScreen from "../screen/Dashboard/DashboardScreen";
import AddBlog from "../screen/Blogs/AddBlog";
import AddCourse from "../screen/Courses/AddCourse";
import AddSlider from "../screen/Sliders/AddSlider";
import UserScreen from "../screen/Users/UserScreen";
import AddSeminar from "../screen/Seminar/AddSeminar";
import SeminarScreen from "../screen/Seminar/SeminarScreen";
import AudioBookScreen from "../screen/AudioBook/AudioBookScreen";
import AddAudioBook from "../screen/AudioBook/AddAudioBook";
import AddIntroductionJob from "../screen/IntroductionJob/AddIntroductionJob";
import IntroductionJobScreen from "../screen/IntroductionJob/IntroductionJobScreen";
import AdminScreen from "../screen/Admin/AdminScreen";
import AddAdmin from "../screen/Admin/AddAdmin";
import ReportScreen from "../screen/Reports/ReportScreen";
import AddGift from "../screen/Gift/AddGift";

const formatRoute = [
  { path: routes.GS_ADMIN_DASHBOARD, component: DashboardScreen },
  { path: routes.GS_ADMIN_COURSES, component: CourseScreen },
  { path: routes.GS_ADMIN_ADD_COURSE, component: AddCourse },
  { path: routes.GS_ADMIN_CATEGORIES, component: CategoryScreen },
  { path: routes.GS_ADMIN_BLOG, component: BlogsScreen },
  { path: routes.GS_ADMIN_ADD_BLOG, component: AddBlog },
  { path: routes.GS_ADMIN_SLIDER, component: SliderScreen },
  { path: routes.GS_ADMIN_ADD_SLIDER, component: AddSlider },
  { path: routes.GS_ADMIN_USERS, component: UserScreen },
  { path: routes.GS_ADMIN_ADD_SEMINAR, component: AddSeminar },
  { path: routes.GS_ADMIN_SEMINAR, component: SeminarScreen },
  { path: routes.GS_ADMIN_AUDIO_BOOK, component: AudioBookScreen },
  { path: routes.GS_ADMIN_ADD_AUDIO_BOOK, component: AddAudioBook },
  { path: routes.GS_ADMIN_ADD_INTRODUCTION_JOBS, component: AddIntroductionJob },
  { path: routes.GS_ADMIN_INTRODUCTION_JOBS, component: IntroductionJobScreen },
  { path: routes.GS_ADMIN_ADMIN, component: AdminScreen },
  { path: routes.GS_ADMIN_ADD_ADMIN, component: AddAdmin },
  { path: routes.GS_ADMIN_REPORTS, component: ReportScreen },
  { path: routes.GS_ADMIN_ADD_GIFT, component: AddGift },
];
export default formatRoute;
