import React from "react";

const dictionary = (text) => {
  let translated;
  let lowerText = text.toLowerCase();
  switch (lowerText) {
    case "BLOG":
      translated = "بلاگ";
      break;
    case "COURSE":
      translated = "دوره";
      break;
    case "Course":
      translated = "دوره";
      break;
    case "Seminar":
      translated = "سمینار";
      break;
    case "AudioBook":
      translated = "کتاب صوتی";
      break;
    case "Publisher":
      translated = "ناشر ";
      break;
    case "Admin":
      translated = "ادمین ";
      break;
    case "job":
      translated = "معرفی مشاغل ";
      break;
    default:
      break;
  }
  return translated;
};

export default dictionary;
