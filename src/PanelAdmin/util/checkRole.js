import Cookie from "js-cookie";
const checkRole = () => {
  return Cookie.get("andisheRole") === "Admin";
};

export default checkRole;
