import axios from "../axios-orders";
import Cookies from "js-cookie";
import Strings from "../../value/PanelString";
import toastify from "../../util/toastify";
import routes from "../../value/routes";
import checkRole from "../../util/checkRole";

const login = async (param, setLoading) => {
  let URL = Strings.ApiString.LOGIN;
  //console.log({ param });
  // setLoading(true);
  let Route = routes.GS_PANEL_ADMIN_TITLE;

  return axios
    .post(URL, param)
    .then((Response) => {
      console.log({ Response });

      if (Response.data) {
        // Cookies.set("andisheToken", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwaG9uZU51bWJlciI6IjA5Mzc5NTE0Nzg2IiwiaWF0IjoxNTg3MTE0NzY4LCJleHAiOjE2MTk1MTQ3Njh9.Fvs-i5pcq4m5NuAvk9JJeqmIAZ5j9nCTCb6h30DW9vM", { expires: 7 });
        // Cookies.set("andisheRole", "asd", { expires: 7 });

        Cookies.set("andisheToken", Response.data.token, { expires: 7 });
        Cookies.set("andisheRole", Response.data.role, { expires: 7 });
        toastify("شما تایید شده اید", "success");
        if (!checkRole()) {
          Route = routes.GS_ADMIN_REPORTS;
        }
        setTimeout(() => {
          window.location = Route;
        }, 1000);

        return true;
      }
    })
    .catch((error) => {
      console.log({ error });
      setLoading(false);

      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else if (error.response.data.Error === 1019) toastify("این شماره ثبت نشده است", "error");
      else toastify("خطایی در سرور . لطفا دوباره تلاش کنید", "error");

      return false;
    });
};
export default login;
