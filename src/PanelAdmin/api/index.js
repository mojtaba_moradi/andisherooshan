import get from "./Get";
import put from "./Put";
import post from "./Post";
import deletes from "./Delete";

export { get, post, put, deletes };
