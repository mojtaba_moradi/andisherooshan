import axios from "../axios-orders";
import Strings from "../../value/PanelString";
import toastify from "../../util/toastify";

const dashboard = async (returnData, page, loading) => {
  // console.log("dashboard");

  loading(true);
  return axios
    .get(Strings.ApiString.DASHBOARD + "/" + page)
    .then((dashboard) => {
      console.log({ dashboard });
      returnData(dashboard.data);
      loading(false);
    })
    .catch((error) => {
      console.log({ error });

      let data = error.response ? error.response.data : "";
      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else if (data)
        if (data.CODE)
          switch (data.CODE) {
            case 1098:
              toastify("شما نمی توانید به این قسمت ورود کنید", "error");
              break;
            default:
              toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
              break;
          }
      return false;
    });
};

export default dashboard;
