import courses from "./courses";
import categoreis from "./categoreis";
import blogs from "./blogs";
import sliders from "./sliders";
import searchCourse from "./searchCourse";
import searchBlog from "./searchBlog";
import users from "./users";
import seminars from "./seminars";
import audioBooks from "./audioBooks";
import introductionJob from "./introductionJob";
import searchIntroductionJob from "./searchIntroductionJob";
import admins from "./admins";
import searchPublisher from "./searchPublisher";
import reports from "./reports";
import dashboard from "./dashboard";
import seenSection from "./seenSection";
import searchAudioBook from "./searchAudioBook";
import searchSeminar from "./searchSeminar";
import searchUser from "./searchUser";

const get = {
  courses,
  categoreis,
  blogs,
  sliders,
  searchCourse,
  searchBlog,
  users,
  seminars,
  audioBooks,
  introductionJob,
  searchIntroductionJob,
  admins,
  searchPublisher,
  reports,
  dashboard,
  seenSection,
  searchAudioBook,
  searchSeminar,
  searchUser,
};
export default get;
