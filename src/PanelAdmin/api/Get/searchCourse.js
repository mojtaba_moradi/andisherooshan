import axios from "../axios-orders";
import Strings from "../../value/PanelString";
import toastify from "../../util/toastify";

const searchCourse = async (returnData, param) => {
  console.log({ param });

  return axios
    .get(Strings.ApiString.COURSE_SEARCH + "/" + param)
    .then(searchCourse => {
      console.log({ searchCourse });
      returnData(searchCourse.data);
    })
    .catch(error => {
      console.log({ error });
      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    });
};

export default searchCourse;
