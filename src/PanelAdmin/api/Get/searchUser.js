import axios from "../axios-orders";
import Strings from "../../value/PanelString";
import toastify from "../../util/toastify";

const searchUser = async (returnData, page, param) => {
  console.log({ returnData, page, param });

  return axios
    .get(Strings.ApiString.USER_SEARCH + "/" + param + "/" + page)
    .then((searchUser) => {
      console.log({ searchUser });
      returnData(searchUser.data);
    })
    .catch((error) => {
      console.log({ error });
      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    });
};

export default searchUser;
