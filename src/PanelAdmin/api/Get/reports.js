import axios from "../axios-orders";
import Strings from "../../value/PanelString";
import toastify from "../../util/toastify";

const reports = async (returnData, page, loading) => {
  loading(true);
  return axios
    .get(Strings.ApiString.REPORT + "/" + page)
    .then((reports) => {
      console.log({ reports });
      returnData(reports.data);
      loading(false);
      return true;
    })
    .catch((error) => {
      //console.log({ error });

      let data = error.response ? error.response.data : false;
      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else if (data)
        if (data.CODE)
          switch (data.CODE) {
            case 1098:
              toastify("شما نمی توانید به این قسمت ورود کنید", "error");
              break;
            default:
              toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
              break;
          }
        else toastify("ارتباط بر قرار نشد", "error");
      return false;
    });
};

export default reports;
