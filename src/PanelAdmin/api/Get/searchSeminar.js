import axios from "../axios-orders";
import Strings from "../../value/PanelString";
import toastify from "../../util/toastify";

const searchSeminar = async (returnData, param) => {
  console.log({ param });

  return axios
    .get(Strings.ApiString.SEMINAR_SEARCH + "/" + param)
    .then((searchSeminar) => {
      console.log({ searchSeminar });
      returnData(searchSeminar.data);
    })
    .catch((error) => {
      console.log({ error });
      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    });
};

export default searchSeminar;
