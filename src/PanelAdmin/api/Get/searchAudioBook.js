import axios from "../axios-orders";
import Strings from "../../value/PanelString";
import toastify from "../../util/toastify";

const searchAudioBook = async (returnData, param) => {
  console.log({ param });

  return axios
    .get(Strings.ApiString.AUDIO_BOOK_SEARCH + "/" + param)
    .then((searchAudioBook) => {
      console.log({ searchAudioBook });
      returnData(searchAudioBook.data);
    })
    .catch((error) => {
      console.log({ error });
      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    });
};

export default searchAudioBook;
