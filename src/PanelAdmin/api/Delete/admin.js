import axios from "../axios-orders";
import Strings from "../../value/PanelString";
import toastify from "../../util/toastify";

const admin = async (param) => {
  let URL = Strings.ApiString.ADMINS;
  return axios
    .delete(URL + "/" + param)
    .then((Response) => {
      console.log({ Response });
      // setLoading(false);
      if (Response.data);
      toastify("با موفقیت حذف شد", "success");
      return true;
    })
    .catch((error) => {
      console.log({ error });

      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else if (error.response.data)
        switch (error.response.data.Error) {
          case 1099:
            toastify("شما قادر به حذف ادمین اصلی نمی باشید ", "error");
            break;
          default:
            toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
            break;
        }
      return false;
    });
};
export default admin;
