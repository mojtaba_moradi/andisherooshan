const ALBUM = "album";
const PLAY_LIST = "playList";
const SONG = "song";
const FLAG = "flag";
const ARTIST = "artist";

const constants = {
  ALBUM,
  PLAY_LIST,
  SONG,
  FLAG,
  ARTIST,
};

export default constants;
