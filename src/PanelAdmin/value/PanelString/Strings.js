// ================= SIDEBAR STRING ==============
const DASHBOARD = "داشبورد";
const PRODUCTS = "محصولات";
const ADD_PRODUCTS = " ثبت محصولات";
const DELETE_PRODUCTS = " حذف محصولات";
const SETTING = "تنظیمات";
const SETTING_WEB = "تنظیمات سایت";
const SIDEBAR_ONE_TITLE = "عمومی";
const SIDEBAR_TWO_TITLE = "کاربردی";
const COURSES = "دوره ";
const SEE_COURSES = " مشاهده دوره ها";
const ADD_COURSE = "افزودن دوره ";
const CATEGORIES = "دسته بندی ";
const SEE_CATEGORY = "مشاهده دسته بندی ها";
const BLOGS = "بلاگ ";
const SEE_BLOGS = " مشاهده بلاگ ها";
const ADD_BLOG = "افزودن بلاگ ";
const SLIDERS = "اسلایدر";
const SEE_SLIDERS = " مشاهده اسلایدرها";
const ADD_SLIDER = " افزودن اسلایدر";
const ABOUT = "درباره";
const TITLE = "عنوان";
const DESCRIPTION = "توضیحات";
const CATEGORY = "دسته بندی";
const ABOUT_COURSE = "درباره دوره ";
const ABOUT_BLOG = "درباره بلاگ ";
const MOBILE_NUMBER = "شماره همراه";
const MEMBER = " کاربر";
const SEE_MEMBERS = "مشاهده کاربران";
const SEMINAR = " سمینار";
const SEE_SEMINAR = "مشاهده سمینار";
const ADD_SEMINAR = "افزودن سمینار";

const AUDIO_BOOK = " کتاب صوتی";
const SEE_AUDIO_BOOKS = "مشاهده کتاب صوتی";
const ADD_AUDIO_BOOK = "افزودن کتاب صوتی";

const INTRODUCTION_JOB = " معرفی مشاغل";
const SEE_INTRODUCTION_JOBS = "مشاهده معرفی مشاغل";
const ADD_INTRODUCTION_JOB = "افزودن معرفی مشاغل";

const ADMIN = " ادمین";
const SEE_ADMIN = "مشاهده ادمین ها";
const ADD_ADMIN = "افزودن ادمین";

const REPORT = "گزارشات";
const SEE_REPORTS = "مشاهده گزارشات";

const GIFT = "جوایز";
const ADD_GIFT = "اهدای جوایز";
// ================= SIDEBAR STRING END==============
const Strings = {
  DASHBOARD,
  PRODUCTS,
  ABOUT,
  TITLE,
  DESCRIPTION,
  SETTING,
  SIDEBAR_ONE_TITLE,
  SIDEBAR_TWO_TITLE,
  ADD_PRODUCTS,
  DELETE_PRODUCTS,
  SETTING_WEB,
  // ========================= category
  CATEGORIES,
  SEE_CATEGORY,
  CATEGORY,
  // ========================= slider
  SLIDERS,
  SEE_SLIDERS,
  ADD_SLIDER,
  // ========================= course
  COURSES,
  ADD_COURSE,
  SEE_COURSES,
  ABOUT_COURSE,
  // ========================= blog
  ABOUT_BLOG,
  SEE_BLOGS,
  ADD_BLOG,
  BLOGS,
  // ========================= member
  MOBILE_NUMBER,
  MEMBER,
  SEE_MEMBERS,
  // ========================= seminar
  SEMINAR,
  SEE_SEMINAR,
  ADD_SEMINAR,
  // ========================= audio book
  AUDIO_BOOK,
  SEE_AUDIO_BOOKS,
  ADD_AUDIO_BOOK,
  // ====================================== introduction job
  INTRODUCTION_JOB,
  SEE_INTRODUCTION_JOBS,
  ADD_INTRODUCTION_JOB,
  // ======================================  admin
  ADMIN,
  SEE_ADMIN,
  ADD_ADMIN,
  REPORT,
  SEE_REPORTS,
  // ======================================  GIFT
  GIFT,
  ADD_GIFT,
};
export default Strings;
