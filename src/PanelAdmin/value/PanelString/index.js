import ApiString from "./ApiString";
import Strings from "./Strings";
import color from "./color";
const PanelString = { Strings, ApiString, color };
export default PanelString;
