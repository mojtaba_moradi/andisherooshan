import React, { useEffect, useState } from "react";
import { get } from "../../../api";
import CardDashboard from "../../../components/CardDashboard";
import routes from "../../../value/routes";
import checkRole from "../../../util/checkRole";

const DashboardScreen = (props) => {
  const [dashboard, setDashboard] = useState();
  const [Loading, setLoading] = useState(false);
  const [state, setState] = useState({ name: "", data: [], dataHead: [], allSoldSeminar: null, allSoldAudioBook: null, allSoldCourse: null });
  console.log({ dashboard });

  useEffect(() => {
    if (!checkRole()) {
      props.history.push(routes.GS_ADMIN_REPORTS);
    } else handelgetApi();
  }, []);
  const handelgetApi = () => {
    get.dashboard(setDashboard, "", setLoading);
  };
  const dashboardCarddata = [
    {
      titleTop: dashboard && dashboard.totalSoldSeminar,
      titleBottom: "سمینار",
      icon: "fas fa-presentation",
      style: { backgroundColor: "#42a5f5" },
    },
    { titleTop: dashboard && dashboard.totalSoldAudioBook, titleBottom: "کتاب صوتی", icon: "far fa-file-audio", style: { backgroundColor: "#ffc107" } },
    { titleTop: dashboard && dashboard.totalSoldCourse, titleBottom: "دوره", icon: "far fa-video", style: { backgroundColor: "#66bb6a" } },
  ];
  const dashboardCardData_map = dashboardCarddata.map((cardData, index) => {
    return <CardDashboard style={cardData.style} key={index + Date()} icon={cardData.icon} titleTop={cardData.titleTop} titleBottom={cardData.titleBottom} />;
  });

  return (
    <div className="countainer-main hight100 centerAll">
      <div className="headtitle "></div>
      <div className=" flexWrap flex width100 ">{dashboardCardData_map}</div>
    </div>
  );
};

export default DashboardScreen;
