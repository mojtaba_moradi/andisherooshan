import React, { useState, useEffect } from "react";
import FormInputAdmin from "./FormInputAdmin";
import onChanges from "../../../util/onChanges";
import states from "../../../util/consts/states";
import { post, put } from "../../../api";

const AddAdmin = (props) => {
  const { editData, modalShow, addNewSection, onHideModal } = props;
  const [data, setData] = useState({ ...states.addAdmin });
  const [state, setState] = useState({ progressPercentImage: null, remove: { index: "", name: "" } });
  const [removeStateData, setRemoveStateData] = useState("");
  const [Loading, setLoading] = useState(false);
  useEffect(() => {
    if (!modalShow) setData({ ...states.addAdmin });
  }, [modalShow]);

  const [checkSubmited, setCheckSubmited] = useState(false);
  // ============================= submited
  const submited = () => setCheckSubmited(!checkSubmited);
  const _onSubmited = async (e) => {
    e.preventDefault();
    let apiRoute, param;
    const formData = {};
    for (let formElementIdentifier in data.Form) if (formElementIdentifier !== removeStateData) formData[formElementIdentifier] = data.Form[formElementIdentifier].value;

    if (editData) {
      apiRoute = put.editSection(formData);
    } else apiRoute = put.addAdmin(formData);
    if (await apiRoute) {
      submited();
      setData({ ...states.addAdmin });
      if (editData) onHideModal();
    }
    console.log(formData);
  };
  // ========================= End submited =================

  const inputChangedHandler = async (event, initial, array) => {
    console.log({ event, initial, array });
    let value = event.value;
    // let datas = initial && !editData ? states.addSlider : data;
    let validName;
    if (initial || array) {
      if (event.length > 0) {
        let val = event.filter((d) => {
          if (d.name === "role") return d.value;
        });
        value = val[0].value;
      }
      if (value === "Admin") validName = "pubType";

      if (!array) submited();
    }
    if (validName) setRemoveStateData(validName);

    await onChanges.globalChange({ event, data, setData, state, setState, setLoading, imageType: "sliders", validName });
  };
  useEffect(() => {
    let arrayData = [];

    if (editData)
      for (const key in editData)
        for (let index = 0; index < stateArray.length; index++) {
          if (stateArray[index].id === key) {
            console.log({ key });

            // if (editData[key] === "VIDEO") {
            //   arrayData.push({ name: "dataUrl", value: editData["video"] ? editData["video"] : "" });
            // } else if (editData[key] === "VOICE") arrayData.push({ name: "dataUrl", value: editData["voice"] ? editData["voice"] : "" });
            arrayData.push({ name: key, value: editData[key] ? editData[key].toString() : editData[key] ? editData[key] : "" });
          }
        }
    console.log({ editData });

    if (arrayData.length > 0) inputChangedHandler(arrayData, false, { value: editData.parentType });
  }, [editData]);

  const stateArray = [];
  for (let key in data.Form) stateArray.push({ id: key, config: data.Form[key] });
  let form = (
    <FormInputAdmin
      _onSubmited={_onSubmited}
      stateArray={stateArray}
      data={data}
      state={state}
      setData={setData}
      Loading={Loading}
      setLoading={setLoading}
      inputChangedHandler={inputChangedHandler}
      checkSubmited={checkSubmited}
      editData={editData}
    />
  );
  return (
    <div className="countainer-main centerAll ">
      <div className="form-countainer">
        <div className="form-subtitle">{editData ? "تغییر در ادمین " : "افزودن ادمین جدید"}</div>
        <div className="row-give-information">
          {form}
          <div className="btns-container">
            <button className="btns btns-primary" disabled={!data.formIsValid} onClick={_onSubmited}>
              افزودن{" "}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddAdmin;
