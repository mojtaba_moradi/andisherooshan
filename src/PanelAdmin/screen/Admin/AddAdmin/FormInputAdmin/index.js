import React from "react";
import "./index.scss";
import { ButtonGroup, Button } from "react-bootstrap";
import Inputs from "../../../../components/UI/Inputs/Input";
const FormInputAdmin = (props) => {
  const { stateArray, removeHandel, state, _onSubmited, inputChangedHandler, checkSubmited, editData, staticTitle } = props;
  let pubTypeData = [
    { value: "AudioBook", title: "کتاب صوتی" },
    { value: "Seminar", title: "سمینار" },
    { value: "Course", title: "دوره" },
  ];
  return (
    <form onSubmit={_onSubmited}>
      {stateArray.map((formElement) => {
        const invalid = !formElement.config.valid;
        const shouldValidate = formElement.config.validation;
        const touched = formElement.config.touched;
        let changed,
          display,
          accepted,
          progress,
          parentType = stateArray[0].config.value,
          disabled = parentType ? false : true;
        if (formElement.id === "pubType") parentType === "Admin" ? (display = "none") : (display = "");
        if (state.progressPercentPlay || state.progressPercentImage) disabled = true;
        const inputClasses = ["InputElement"];
        if (invalid && shouldValidate && touched) inputClasses.push("Invalid");
        else if (formElement.id === "cover") progress = state.progressPercentImage;
        changed = (e) => inputChangedHandler({ value: e.currentTarget.value, name: formElement.id, type: e.currentTarget.type, files: e.currentTarget.files });
        accepted = (value) => inputChangedHandler({ value: value, name: formElement.id });

        let form = (
          <Inputs
            key={formElement.id}
            elementType={formElement.config.elementType}
            elementConfig={formElement.config.elementConfig}
            value={formElement.config.value}
            invalid={invalid}
            shouldValidate={shouldValidate}
            touched={touched}
            changed={changed}
            accepted={accepted}
            removeHandel={(index) => removeHandel(index, formElement.id)}
            label={formElement.config.label}
            progress={progress}
            dropDownData={""}
            checkSubmited={checkSubmited}
            disabled={disabled}
            display={display}
            defaultInputDesable={true}
            dropDownData={pubTypeData}
            staticTitle={staticTitle && staticTitle.name === formElement.id ? staticTitle.value : ""}
          />
        );
        if (formElement.id === "role") {
          form = (
            <div className={"Input"}>
              <label className={"Label"}>{formElement.config.label}</label>
              <ButtonGroup style={{ direction: "ltr", width: "100%" }} aria-label="Basic example">
                {formElement.config.childValue.map((child, index) => {
                  return (
                    <Button
                      disabled={editData}
                      onClick={() => inputChangedHandler({ value: child.value, name: formElement.id }, { initialState: true })}
                      style={{
                        backgroundColor: formElement.config.value === child.value ? "#07a7e3" : "",
                      }}
                      key={index}
                      variant="secondary"
                    >
                      {child.name}{" "}
                    </Button>
                  );
                })}
              </ButtonGroup>
            </div>
          );
        }
        return form;
      })}
    </form>
  );
};

export default FormInputAdmin;
