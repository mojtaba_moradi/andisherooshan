import React, { useEffect, useState } from "react";
import { get } from "../../../api";
import SpinnerRotate from "../../../util/Loadings/SpinnerRotate";
import table from "../../../util/consts/table";
import DropdownBoot from "../../../components/UI/Inputs/DropdownBoot";
import TabelBasic from "../../../components/Tables";
import IsNull from "../../../components/IsNull";
import checkRole from "../../../util/checkRole";
import formatMoney from "../../../util/formatMoney";
const ReportScreen = () => {
  const [Reports, setReports] = useState();
  const [Loading, setLoading] = useState(false);
  const [state, setState] = useState({ name: "", data: [], dataHead: [], allSoldSeminar: null, allSoldAudioBook: null, allSoldCourse: null, totalSales: null });
  // console.log({ Reports });
  let roleAdmin = checkRole();

  useEffect(() => {
    handelgetApi();
  }, []);
  const handelgetApi = () => {
    get.reports(setReports, "", setLoading);
  };
  useEffect(() => {
    if (Reports && roleAdmin) {
      setState({
        ...state,
        name: "دوره",
        data: table.reportsSoldCourse(Reports.soldCourses)[1],
        dataHead: table.reportsSoldCourse(Reports.soldCourses)[0],
        allSoldSeminar: { data: table.reportsSoldSeminar(Reports.soldSeminars)[1], dataHead: table.reportsSoldSeminar(Reports.soldSeminars)[0] },
        allSoldAudioBook: { data: table.reportsSoldCourse(Reports.soldAudioBooks)[1], dataHead: table.reportsSoldCourse(Reports.soldAudioBooks)[0] },
        allSoldCourse: { data: table.reportsSoldCourse(Reports.soldCourses)[1], dataHead: table.reportsSoldCourse(Reports.soldCourses)[0] },
      });
    } else if (Reports && !roleAdmin) {
      let stateVal;

      if (Reports.allSales[0].seminar)
        stateVal = {
          dataHead: table.reportsSoldSeminarPublisher(Reports.allSales)[0],
          data: table.reportsSoldSeminarPublisher(Reports.allSales)[1],
        };
      else if (Reports.allSales[0].course)
        stateVal = {
          dataHead: table.reportsSoldCoursePublisher(Reports.allSales)[0],
          data: table.reportsSoldCoursePublisher(Reports.allSales)[1],
        };
      else if (Reports.allSales[0].audioBook) {
        stateVal = {
          dataHead: table.reportsSoldAudioBookPublisher(Reports.allSales)[0],
          data: table.reportsSoldAudioBookPublisher(Reports.allSales)[1],
        };
      }
      setState({
        ...state,
        ...stateVal,
        // totalSales: "صورتحساب کلی شما :" + formatMoney(Reports.totalSales),
      });
    }

    // console.log({ allSoldSeminar, allSoldAudioBook, allSoldCourse });
  }, [Reports]);

  console.log({ state });
  console.log({ Reports });

  const dropDown = [{ title: "کتاب صوتی" }, { title: "دوره" }, { title: "سمینار" }];
  let dropDownData = [];
  for (const index in dropDown) dropDownData.push({ value: dropDown[index].title, title: dropDown[index].title });
  const accepted = (value) => {
    let data;
    switch (value) {
      case "سمینار":
        data = state.allSoldSeminar;
        break;
      case "کتاب صوتی":
        data = state.allSoldAudioBook;
        break;
      case "دوره":
        data = state.allSoldCourse;
        break;
      default:
        break;
    }
    setState({ ...state, name: value, ...data });
  };
  const showDataElement = (
    <div className="countainer-main centerAll scale-up-ver-top">
      <div className="elemnts-box  boxShadow">
        <div className="subtitle-elements">
          <span className="centerAll"> {"گزارشات"}</span>
          <div className="btns-container">{roleAdmin ? <DropdownBoot dropDownData={dropDownData} accepted={accepted} value={state.name} /> : state.totalSales}</div>
        </div>

        {state.data.length ? <TabelBasic imgStyle={{ width: "2em", height: "2em", borderRadius: "0.4em" }} tbody={state.data} thead={state.dataHead} /> : <IsNull title={"خالی می باشد"} />}
      </div>
    </div>
  );
  return Loading ? (
    <div className="staticStyle bgWhite">
      <SpinnerRotate />
    </div>
  ) : (
    showDataElement
  );
};
export default ReportScreen;
