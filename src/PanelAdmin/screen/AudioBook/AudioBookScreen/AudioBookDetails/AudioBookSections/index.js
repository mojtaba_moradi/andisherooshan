import React, { useState, useEffect, Fragment } from "react";
import "./index.scss";
import PlayBox from "../../../../../components/PlayBox";
import IsNull from "../../../../../components/IsNull";
const AudioBookSections = ({ sections, onShowModalInputs, handelEdit, handelRemove }) => {
  const [state, setState] = useState({
    voice: [],
  });
  useEffect(() => {
    setState((prev) => ({ ...prev, voice: sections }));
  }, [sections]);

  const elements = (
    <Fragment>
      <div className="card-details-row">
        <div className="courses-title">
          <span>{"قسمت ها"}</span>
          <div className="centerAll">({state.voice.length})</div>
          <div className="add-new-Course">
            <button type="button" className="btn btn-primary" onClick={() => onShowModalInputs({ kindOf: "add" })}>
              افزودن
            </button>{" "}
          </div>
        </div>
        <div className="show-courses-container">
          <div className="show-courses">
            {state.voice.length > 0 ? (
              <PlayBox playSrc={(event) => onShowModalInputs({ ...event, kindOf: "src" })} handelEdit={handelEdit} data={state.voice} name={"VOICE"} handelRemove={handelRemove} />
            ) : (
              <IsNull title={"خالی می باشد"} />
            )}
          </div>
        </div>
      </div>
    </Fragment>
  );
  return <Fragment>{elements}</Fragment>;
};

export default AudioBookSections;
