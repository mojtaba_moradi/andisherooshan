import React, { useEffect, useState, Fragment } from "react";
import { get, put, deletes } from "../../../api";
import BoxLoading from "react-loadingg/lib/BoxLoading";
import ShowCardInformation from "../../../components/ShowCardInformation";
import AudioBookDetails from "./AudioBookDetails";
import card from "../../../util/consts/card";
import ModalBox from "../../../util/modals/ModalBox";
import ModalTrueFalse from "../../../util/modals/ModalTrueFalse";
import Pageination from "../../../components/UI/Pagination";
import SpinnerRotate from "../../../util/Loadings/SpinnerRotate";

const AudioBookScreen = () => {
  const [AudioBooks, setAudioBooks] = useState();
  const [loading, setLoading] = useState(true);
  const [acceptedIndex, setAcceptedIndex] = useState();
  const [state, setState] = useState();
  const [editData, setEditData] = useState(false);
  const [ModalInpts, setModalInpts] = useState({
    show: false,
    type: false,
  });
  let index = 0;
  useEffect(() => {
    _handelShowAudioBook(acceptedIndex);
  }, [state]);
  useEffect(() => {
    apiAudioBooks(1);
  }, []);
  const apiAudioBooks = async (page) => await get.audioBooks(setAudioBooks, page ? page : "1", setLoading);

  const refreshComponent = async () => {
    if (await apiAudioBooks(AudioBooks.page)) {
      setState(index++);
    }
  };
  const _handelShowAudioBook = (index) => setAcceptedIndex(index);

  const _handelback = () => {
    setAcceptedIndex();
    apiAudioBooks(AudioBooks.page);
  };
  const sendNewValData = async (param) => {
    let Info = await put.audioBook(param);
    if (Info) refreshComponent();
  };

  const optionClick = async (event) => {
    switch (event.mission) {
      case "remove":
        removeHandel(event._id);
        break;
      default:
        break;
    }
  };

  // =========================== modal
  // ============================= remove
  const __returnPrevstep = async (value) => {
    onHideModal();
    if (value) if (await deletes.audioBook(state.remove.id)) refreshComponent();

    setState({ ...state, remove: { index: "", name: "" } });
  };
  const removeHandel = (id, name) => {
    onShowlModal(true);
    setState({ ...state, remove: { id, name } });
  };

  // =========================== End remove  ====================
  const onHideModal = () => {
    setModalInpts({ ...ModalInpts, show: false });
    refreshComponent();
    setEditData();
  };
  const onShowlModal = (type) => {
    if (type) setModalInpts({ ...ModalInpts, show: true, type: true });
    else setModalInpts({ ...ModalInpts, show: true, type: false });
  };
  const renderModalInputs = (
    <ModalBox onHideModal={onHideModal} showModal={ModalInpts.show}>
      <ModalTrueFalse modalHeadline={"آیا مطمئن به حذف آن هستید !"} modalAcceptTitle={"بله"} modalCanselTitle={"خیر"} modalAccept={__returnPrevstep} />
    </ModalBox>
  );
  const _handelPage = (value) => {
    if (value) apiAudioBooks(value);
  };
  //console.log({ AudioBooks, Loading });
  const showDataElement =
    AudioBooks &&
    AudioBooks.docs.length > 0 &&
    (acceptedIndex >= 0 ? (
      <AudioBookDetails information={AudioBooks.docs[acceptedIndex]} handelback={_handelback} refreshComponent={refreshComponent} sendNewValData={sendNewValData} />
    ) : (
      <ShowCardInformation data={card.audioBook(AudioBooks.docs)} options={{ remove: true }} onClick={_handelShowAudioBook} optionClick={optionClick} submitedTitle="مشاهده بلاگ" />
    ));

  return (
    <Fragment>
      {renderModalInputs}
      <div className="countainer-main">
        {acceptedIndex === undefined && (
          <div className="show-total-data">
            <div>
              {" "}
              {"تعداد کل :"} <span>{AudioBooks ? AudioBooks.total : 0}</span>
            </div>
            <div>
              {" "}
              {"تعداد نمایش :"} <span>{AudioBooks ? AudioBooks.docs.length : 0}</span>
            </div>
          </div>
        )}
        {showDataElement}
        {AudioBooks && acceptedIndex === undefined && AudioBooks.pages >= 2 && (
          <Pageination limited={"3"} pages={AudioBooks ? AudioBooks.pages : ""} activePage={AudioBooks ? AudioBooks.page : ""} onClick={_handelPage} />
        )}
      </div>
      {loading && (
        <div className="staticStyle bgWhite">
          <SpinnerRotate />
        </div>
      )}
    </Fragment>
  );
};

export default AudioBookScreen;
