import React, { useEffect, useState } from "react";
import "./index.scss";
import { get, post, deletes } from "../../../api";
import { BoxLoading } from "react-loadingg";
import ModalBox from "../../../util/modals/ModalBox";
import AddCategory from "../AddCategory";
import SpinnerRotate from "../../../util/Loadings/SpinnerRotate";
import ModalTrueFalse from "../../../util/modals/ModalTrueFalse";
const CategoryScreen = () => {
  const [Categories, setCategories] = useState([]);
  const [Loading, setLoading] = useState(true);
  const [state, setState] = useState({ remove: { index: "", name: "" } });
  const [editData, setEditData] = useState(false);
  const [InitialState, setInitialState] = useState(false);
  const [ModalInpts, setModalInpts] = useState({
    show: false,
    type: false,
  });
  useEffect(() => {
    handelgetApi();
  }, []);
  const handelgetApi = () => {
    get.categoreis(setCategories, setLoading);
  };
  // =========================== modal
  // ============================= remove
  const __returnPrevstep = async (value) => {
    onHideModal();
    if (value) if (await deletes.category(state.remove.id)) handelgetApi();
    setState({ ...state, remove: { index: "", name: "" } });
  };
  const removeHandel = (id, name) => {
    onShowlModal(true);
    setState({ ...state, remove: { id, name } });
  };
  // =========================== End remove  ====================
  const onHideModal = () => {
    setModalInpts({ ...ModalInpts, show: false, type: false });
    handelgetApi();
    setEditData();
  };
  const onShowlModal = (type) => {
    if (type) setModalInpts({ ...ModalInpts, show: true, type: true });
    else setModalInpts({ ...ModalInpts, show: true, type: false });
  };
  const renderModalInputs = (
    <ModalBox onHideModal={onHideModal} showModal={ModalInpts.show}>
      {ModalInpts.type ? (
        <ModalTrueFalse modalHeadline={"آیا مطمئن به حذف آن هستید !"} modalAcceptTitle={"بله"} modalCanselTitle={"خیر"} modalAccept={__returnPrevstep} />
      ) : (
        <AddCategory InitialState={InitialState} onHideModal={onHideModal} handelgetApi={handelgetApi} editData={editData} />
      )}
    </ModalBox>
  );

  const showDataElement = (
    <div className="countainer-main centerAll scale-up-ver-top">
      <div className="elemnts-box  boxShadow">
        <div className="subtitle-elements">
          <span className="centerAll">دسته بندی ها</span>
          <div className="btns-container">
            <button onClick={() => onShowlModal()} className="btns btns-primary">
              افزودن +
            </button>
          </div>
        </div>

        <div className="show-elements">
          {Categories.map((category, index) => {
            return (
              <div key={index + "mo"} onClick={() => removeHandel(category._id, category.title)} className="show-data-title-box transition0-3" key={index}>
                <span>{category.title}</span>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
  return Loading ? (
    <div className="staticStyle bgWhite">
      <SpinnerRotate />
    </div>
  ) : (
    <React.Fragment>
      {renderModalInputs}
      {showDataElement}
    </React.Fragment>
  );
};

export default CategoryScreen;
