// library import
import React, { useState } from "react";
import "./index.css";
// class import

import { post } from "../../api";
import toastify from "../../util/toastify";
import Strings from "../../value/PanelString/Strings";
import LoadingDot1 from "../../util/Loadings/LoadingDot1";

const LoginScreen = (props) => {
  const [phoneNumber, setPhoneNumber] = useState("");
  const [password, setPassword] = useState("");
  const [isLoading, setLoading] = useState(false);
  const onChangePhoneNumber = (e) => {
    const pattern = /^\d+$/;
    if (pattern.test(e.currentTarget.value)) setPhoneNumber(e.currentTarget.value);
    else if (phoneNumber.length === 1) setPhoneNumber(phoneNumber.slice(0, phoneNumber.length - 1));
  };

  const onChangePassword = (e) => {
    setPassword(e.currentTarget.value);
  };

  const onLoginClick = async () => {
    setLoading(true);
    const phoneRegEx = /[0,9]{2}\d{9}/g;
    const regMobile = phoneRegEx.test(phoneNumber);
    let valid = true;
    if (phoneNumber.length === 0) {
      toastify("شماره خود را وارد کنید", "error");
      valid = false;
    } else if (!regMobile) {
      toastify("شماره وارد شده اشتباه است", "error");
      valid = false;
    } else if (password.length === 0) {
      toastify("رمز عبور خود را وارد کنید", "error");
      valid = false;
    } else if (password.length <= 7) {
      toastify("رمز عبور حداقل 8 کاراکتر است", "error");
      valid = false;
    }
    if (valid) {
      let param = { phoneNumber, password };

      post.login(param, setLoading);
    } else setLoading(false);
  };

  return (
    <div className="adminLogin">
      <div className="adminLoginForm">
        <h2>ورود</h2>
        <div className={"adminLoginInput"}>
          <label>{Strings.MOBILE_NUMBER}</label>
          <div>
            {/* <ion-icon name="phone-portrait-outline" size="large" /> */}
            <div>
              <i style={{ transform: "rotate(-100deg)" }} className="icon-phone"></i>
            </div>
            <input type={"txt"} maxlength="11" onChange={onChangePhoneNumber} value={phoneNumber} placeholder=" - - - - - - - - - 09" />
          </div>
        </div>
        <div className={"adminLoginInput"}>
          <label>رمز عبور</label>
          <div>
            <div>
              <i className="icon-key"></i>
            </div>
            {/* <ion-icon name="key-outline" size="large" /> */}
            <input type={"password"} onChange={onChangePassword} value={password} placeholder="* * * * * *" />
          </div>
        </div>
        <button onClick={onLoginClick} disabled={isLoading} type="button" className="btn btn-success">
          {isLoading ? <LoadingDot1 width="30px" height="30px" /> : "ورود"}
        </button>
        {/* <h4 onClick={() => props.history.push("/register")}>ایجاد حساب کاربری</h4> */}
      </div>
    </div>
  );
};

export default LoginScreen;
