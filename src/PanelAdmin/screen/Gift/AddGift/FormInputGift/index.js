import React, { useState, useEffect } from "react";
import Inputs from "../../../../components/UI/Inputs/Input";
import "./index.scss";
import { get } from "../../../../api";
import { ButtonGroup, Button } from "react-bootstrap";
const FormInputSlider = (props) => {
  const { stateArray, removeHandel, state, _onSubmited, inputChangedHandler, checkSubmited, editData, staticTitle } = props;
  const [SearchCourse, setSearchCourse] = useState([]);
  const [SearchSeminar, setSearchSeminar] = useState([]);
  const [SearchAudioBook, setSearchAudioBook] = useState([]);
  const [SearchUser, setSearchUser] = useState([]);
  console.log({ mooj: SearchUser.docs });

  useEffect(() => {
    setSearchCourse([]);
    setSearchSeminar([]);
    setSearchAudioBook([]);
    setSearchUser([]);
  }, [checkSubmited]);
  console.log({ staticTitle });
  let SearchCourseData = [];
  let SearchSeminarData = [];
  let SearchAudioBookData = [];
  let SearchUserData = [];
  for (const index in SearchCourse.docs) SearchCourseData.push({ value: SearchCourse.docs[index]._id, title: SearchCourse.docs[index].title, description: SearchCourse.docs[index].description, image: SearchCourse.docs[index].cover });
  for (const index in SearchSeminar.docs) SearchSeminarData.push({ value: SearchSeminar.docs[index]._id, title: SearchSeminar.docs[index].title, description: SearchSeminar.docs[index].description, image: SearchSeminar.docs[index].cover });
  for (const index in SearchAudioBook.docs) SearchAudioBookData.push({ value: SearchAudioBook.docs[index]._id, title: SearchAudioBook.docs[index].title, description: SearchAudioBook.docs[index].description, image: SearchAudioBook.docs[index].cover });
  for (const index in SearchUser.docs) SearchUserData.push({ value: SearchUser.docs[index]._id, title: SearchUser.docs[index].fullName, description: SearchUser.docs[index].phoneNumber, image: SearchUser.docs[index].avatar });

  const searchedCourse = (e) => {
    setSearchCourse([]);
    if (e.currentTarget.value.length >= 2) get.searchCourse(setSearchCourse, "", e.currentTarget.value);
  };
  const searchedAudioBook = (e) => {
    setSearchCourse([]);
    if (e.currentTarget.value.length >= 2) get.searchAudioBook(setSearchAudioBook, "", e.currentTarget.value);
  };
  const searchedSeminar = (e) => {
    setSearchCourse([]);
    if (e.currentTarget.value.length >= 2) get.searchSeminar(setSearchSeminar, "", e.currentTarget.value);
  };
  const searchedUser = (e) => {
    setSearchCourse([]);
    if (e.currentTarget.value.length >= 2) get.searchUser(setSearchUser, "1", e.currentTarget.value);
  };
  const accept = (event) => {
    setSearchCourse([]);
    inputChangedHandler(event);
  };
  return (
    <form onSubmit={_onSubmited}>
      {stateArray.map((formElement) => {
        const invalid = !formElement.config.valid;
        const shouldValidate = formElement.config.validation;
        const touched = formElement.config.touched;

        let changed,
          display,
          accepted,
          dropDownData,
          parentType = stateArray[0].config.value,
          disabled = parentType ? false : true;

        if (formElement.id === "course") parentType === "course" ? (display = "") : (display = "none");
        if (formElement.id === "audioBook") parentType === "audioBook" ? (display = "") : (display = "none");
        if (formElement.id === "seminar") parentType === "seminar" ? (display = "") : (display = "none");

        const inputClasses = ["InputElement"];
        if (invalid && shouldValidate && touched) inputClasses.push("Invalid");

        if (formElement.id === "type") accepted = (value) => accept({ value, name: formElement.id });
        else if (formElement.id === "course") {
          changed = searchedCourse;
          accepted = (value) => accept({ value, name: formElement.id });
          dropDownData = SearchCourseData;
        } else if (formElement.id === "seminar") {
          changed = searchedSeminar;
          accepted = (value) => accept({ value, name: formElement.id });
          dropDownData = SearchSeminarData;
        } else if (formElement.id === "audioBook") {
          changed = searchedAudioBook;
          accepted = (value) => accept({ value, name: formElement.id });
          dropDownData = SearchAudioBookData;
        } else if (formElement.id === "user") {
          changed = searchedUser;
          accepted = (value) => accept({ value, name: formElement.id });
          dropDownData = SearchUserData;
        } else {
          changed = (e) => inputChangedHandler({ value: e.currentTarget.value, name: formElement.id, type: e.currentTarget.type, files: e.currentTarget.files });
          accepted = (value) => inputChangedHandler({ value: value, name: formElement.id });
        }

        let form = (
          <Inputs
            key={formElement.id}
            elementType={formElement.config.elementType}
            elementConfig={formElement.config.elementConfig}
            value={formElement.config.value}
            invalid={invalid}
            shouldValidate={shouldValidate}
            touched={touched}
            changed={changed}
            accepted={accepted}
            removeHandel={(index) => removeHandel(index, formElement.id)}
            label={formElement.config.label}
            progress={state.progressPercentImage}
            dropDownData={dropDownData}
            checkSubmited={checkSubmited}
            disabled={disabled || state.progressPercentImage}
            display={display}
            defaultInputDesable={true}
            staticTitle={staticTitle.name === formElement.id ? staticTitle.value : ""}
          />
        );
        if (formElement.id === "type") {
          form = (
            <div className={"Input"}>
              <label className={"Label"}>{formElement.config.label}</label>
              <ButtonGroup style={{ direction: "ltr", width: "100%" }} aria-label="Basic example">
                {formElement.config.childValue.map((child, index) => {
                  return (
                    <Button
                      disabled={""}
                      onClick={() => inputChangedHandler({ value: child.value, name: formElement.id }, { initialState: true })}
                      style={{
                        backgroundColor: formElement.config.value === child.value ? "#07a7e3" : "",
                      }}
                      key={index}
                      variant="secondary"
                    >
                      {child.name}{" "}
                    </Button>
                  );
                })}
              </ButtonGroup>
            </div>
          );
        }
        return form;
      })}
    </form>
  );
};

export default FormInputSlider;
