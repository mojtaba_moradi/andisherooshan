import React, { useEffect, useState, Fragment } from "react";
import { get, put, deletes } from "../../../api";
import BoxLoading from "react-loadingg/lib/BoxLoading";
import ShowCardInformation from "../../../components/ShowCardInformation";
import BlogDetails from "./BlogDetails";
import card from "../../../util/consts/card";
import ModalBox from "../../../util/modals/ModalBox";
import ModalTrueFalse from "../../../util/modals/ModalTrueFalse";
import SpinnerRotate from "../../../util/Loadings/SpinnerRotate";
import Pageination from "../../../components/UI/Pagination";

const BlogsScreen = () => {
  const [Blogs, setBlogs] = useState();
  const [Loading, setLoading] = useState(true);
  const [acceptedIndex, setAcceptedIndex] = useState();
  const [state, setState] = useState();
  const [editData, setEditData] = useState(false);
  const [ModalInpts, setModalInpts] = useState({
    show: false,
    type: false,
  });
  let index = 0;
  useEffect(() => {
    _handelShowBlog(acceptedIndex);
  }, [state]);
  useEffect(() => {
    apiBlogs(1);
  }, []);
  const apiBlogs = async (page) => await get.blogs(setBlogs, page ? page : "1", setLoading);

  const refreshComponent = async () => {
    if (await apiBlogs(Blogs.page)) {
      setState(index++);
    }
  };
  const _handelShowBlog = (index) => {
    setAcceptedIndex(index);
  };
  const _handelback = () => {
    setAcceptedIndex();
    apiBlogs(Blogs.page);
  };
  const sendNewValData = async (param) => {
    let Info = await put.editBlog(param);
    if (Info) refreshComponent();
  };

  const optionClick = async (event) => {
    switch (event.mission) {
      case "remove":
        removeHandel(event._id);
        break;

      default:
        break;
    }
  };

  // =========================== modal
  // ============================= remove
  const __returnPrevstep = async (value) => {
    onHideModal();
    if (value) if (await deletes.blog(state.remove.id)) refreshComponent();

    setState({ ...state, remove: { index: "", name: "" } });
  };
  const removeHandel = (id, name) => {
    onShowlModal(true);
    setState({ ...state, remove: { id, name } });
  };

  // =========================== End remove  ====================
  const onHideModal = () => {
    setModalInpts({ ...ModalInpts, show: false });
    refreshComponent();
    setEditData();
  };
  const onShowlModal = (type) => {
    if (type) setModalInpts({ ...ModalInpts, show: true, type: true });
    else setModalInpts({ ...ModalInpts, show: true, type: false });
  };
  const renderModalInputs = (
    <ModalBox onHideModal={onHideModal} showModal={ModalInpts.show}>
      <ModalTrueFalse modalHeadline={"آیا مطمئن به حذف آن هستید !"} modalAcceptTitle={"بله"} modalCanselTitle={"خیر"} modalAccept={__returnPrevstep} />
    </ModalBox>
  );
  const _handelPage = (value) => {
    if (value) apiBlogs(value);
  };
  //console.log({ Blogs, Loading });
  const showDataElement =
    Blogs &&
    Blogs.docs.length > 0 &&
    (acceptedIndex >= 0 ? (
      <React.Fragment>
        <BlogDetails information={Blogs.docs[acceptedIndex]} handelback={_handelback} refreshComponent={refreshComponent} sendNewValData={sendNewValData} />
      </React.Fragment>
    ) : (
      <ShowCardInformation data={card.blog(Blogs.docs)} options={{ remove: true }} onClick={_handelShowBlog} optionClick={optionClick} submitedTitle="مشاهده بلاگ" />
    ));

  return (
    <Fragment>
      {renderModalInputs}
      <div className="countainer-main">
        {acceptedIndex === undefined && (
          <div className="show-total-data">
            <div>
              {" "}
              {"تعداد کل :"} <span>{Blogs ? Blogs.total : 0}</span>
            </div>
            <div>
              {" "}
              {"تعداد نمایش :"} <span>{Blogs ? Blogs.docs.length : 0}</span>
            </div>
          </div>
        )}
        {showDataElement}
        {Blogs && acceptedIndex === undefined && Blogs.pages >= 2 && <Pageination limited={"3"} pages={Blogs ? Blogs.pages : ""} activePage={Blogs ? Blogs.page : ""} onClick={_handelPage} />}
      </div>
      {Loading && (
        <div className="staticStyle bgWhite">
          <SpinnerRotate />
        </div>
      )}
    </Fragment>
  );
};

export default BlogsScreen;
