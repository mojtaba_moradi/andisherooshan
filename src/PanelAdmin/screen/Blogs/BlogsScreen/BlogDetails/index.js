import React, { useState, useEffect, useMemo } from "react";
import "./index.scss";
import BlogSections from "./BlogSections";
import { Button, ButtonToolbar } from "react-bootstrap";
import ModalSrc from "../../../../util/modals/ModalSrc";
import ModalBox from "../../../../util/modals/ModalBox";
import { put, post, get } from "../../../../api";
import DetailsThumbnail from "../../../../components/DetailsComponent/DetailsThumbnail";
import toastify from "../../../../util/toastify";
import DetailsStringTextArea from "../../../../components/DetailsComponent/DetailsStringTextArea";
import DetailsStringInput from "../../../../components/DetailsComponent/DetailsStringInput";
import DetailsInputSearch from "../../../../components/DetailsComponent/DetailsInputSearch";
import InputsType from "../../../../components/UI/Inputs/InputsType";
const BlogDetails = ({ information, handelback, refreshComponent, sendNewValData }) => {
  const [ModalInpts, setModalInpts] = useState({
    show: false,
    files: false,
    kindOf: false,
    data: { src: null, type: null, name: null },
  });
  console.log({ information });

  const stateData = {
    Form: {
      file: {
        label: "تغییر فایل ",
        elementType: "inputFile",
        kindOf: information.type === "VIDEO" ? "video" : "voice",
        value: "",
        validation: {
          required: true,
        },
        valid: false,
        touched: false,
      },
    },
    formIsValid: false,
  };
  const [categories, setCategories] = useState([]);
  const [loading, setLoading] = useState(true);
  let CatSearch = [];
  let CatInfo = { value: information.category ? information.category._id : "", title: information.category ? information.category.title : "" };
  useEffect(() => {
    get.categoreis(setCategories, setLoading);
  }, []);
  for (const index in categories) CatSearch.push({ value: categories[index]._id, title: categories[index].title });
  const modalReturnData = async ({ upload, name }) => {
    console.log({ upload, name });
    let data = { fieldChange: name, newValue: upload };
    sendNewVal(data);
  };
  // ================================================== modal src ==============================
  // ================================================== modal src ==============================
  const onHideModal = () => {
    setModalInpts({ ...ModalInpts, show: false });
    setTimeout(() => {
      setModalInpts({ ...ModalInpts, show: false, data: { src: false, type: false, name: false }, kindOf: false });
    }, 200);
  };
  const onShowlModal = ({ src, name, type, play }) => {
    console.log({ src, name, type, play });
    if (src) setModalInpts({ ...ModalInpts, show: true, data: { src, type, name }, kindOf: "src" });
    else setModalInpts({ ...ModalInpts, show: true, data: { src: false, type: false, name: false }, kindOf: "upload" });
  };

  const renderModalSources = (
    <ModalBox onHideModal={onHideModal} showModal={ModalInpts.show}>
      {ModalInpts.kindOf === "src" ? (
        <div className={"bgUnset"}>
          <ModalSrc played={ModalInpts.data} />
        </div>
      ) : (
        <InputsType showModal={ModalInpts.show} onHideModal={onHideModal} inputsData={(upload) => modalReturnData({ name: "dataUrl", upload })} acceptedTitle={"ثبت"} imageType={"dataUrl"} stateData={stateData} />
      )}
    </ModalBox>
  );
  // =================================================End modal src =================
  // ================================================= modalInputs ======================
  const sendNewVal = async (data) => {
    console.log({ data });
    let param;
    param = Object.assign({ data }, { _id: information._id });
    sendNewValData(param);
  };
  // =================================================End modalInputs ======================
  const elements = (
    <React.Fragment>
      {renderModalSources}
      <div className="show-card-elements-details opacity-Fade-in-adn-slide-top ">
        <ButtonToolbar>
          <Button onClick={handelback} variant="outline-primary">
            بازگشت <span>{">>>"}</span>
          </Button>
        </ButtonToolbar>
        <DetailsThumbnail toastify={toastify} label={"عکس"} elementType={"inputFile"} imageType={"cover"} fieldName={"cover"} cover={information.cover} sendNewVal={sendNewVal} />
        <div className="detailsRow-wrapper">
          <div>
            {" "}
            <DetailsStringInput toastify={toastify} sendNewVal={sendNewVal} Info={information.title} fieldName={"title"} label={"عنوان"} />
            <DetailsStringTextArea toastify={toastify} sendNewVal={sendNewVal} Info={information.description} Id={information._id} fieldName={"description"} label={"توضیحات"} />
            <DetailsInputSearch toastify={toastify} sendNewVal={sendNewVal} Info={CatInfo} searchInfo={CatSearch} fieldName={"category"} label={"دسته بندی  "} />
          </div>
          <div>
            <DetailsStringTextArea toastify={toastify} sendNewVal={sendNewVal} Info={information.aboutBlog} Id={information._id} fieldName={"aboutBlog"} label={"درباره بلاگ"} />
          </div>
        </div>
        <BlogSections toastify={toastify} sendNewVal={sendNewVal} blog={information} onShowlModal={onShowlModal} handelEdit={onShowlModal} />
      </div>
    </React.Fragment>
  );
  return elements;
};

export default BlogDetails;
