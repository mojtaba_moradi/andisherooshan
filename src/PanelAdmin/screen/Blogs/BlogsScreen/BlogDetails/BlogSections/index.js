import React, { useState, useEffect, Fragment } from "react";
import "./index.scss";
import BlogPlayBox from "./BlogPlayBox";
import ModalSrc from "../../../../../util/modals/ModalSrc";
import ModalBox from "../../../../../util/modals/ModalBox";

const BlogSections = ({ blog, handelEdit, onShowlModal }) => {
  const [state, setState] = useState({
    video: [],
    voice: []
  });
  useEffect(() => {
    let video = [];
    let voice = [];

    switch (blog.type) {
      case "VIDEO":
        video.push(blog);
        break;
      case "VOICE":
        voice.push(blog);
        break;
      default:
        break;
    }

    setState(prev => ({ ...prev, video, voice }));
  }, [blog]);

  // //console.log({ video: state.video, voice: state.voice });
  const elements = (
    <Fragment>
      <div className="card-details-row">
        <div className="courses-title">
          <span>بلاگ</span>
          {/* <div className="add-new-Course">
          <button type="button" className="btn btn-primary" onClick={onShowModalInputs}>
            افزودن
          </button>{" "}
        </div> */}
        </div>
        <div className="show-courses-container">
          {state.video.length ? <div className="show-courses">{<BlogPlayBox onShowModalSource={onShowlModal} handelEdit={handelEdit} data={state.video} name={"VIDEO"} />}</div> : ""}
          {state.voice.length ? <div className="show-courses">{<BlogPlayBox onShowModalSource={onShowlModal} handelEdit={handelEdit} data={state.voice} name={"VOICE"} />}</div> : ""}
        </div>
      </div>
    </Fragment>
  );
  return <Fragment>{elements}</Fragment>;
};

export default BlogSections;
