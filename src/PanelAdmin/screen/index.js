import React from "react";
import PanelAdminMain from "../components/PanelAdminMain";
import "./index.scss";
const PanelAdminScreens = () => {
  return (
    <div className="panelAdmin-container">
      <div className="panelAdmin-content">
        <PanelAdminMain />
      </div>
    </div>
  );
};

export default PanelAdminScreens;
