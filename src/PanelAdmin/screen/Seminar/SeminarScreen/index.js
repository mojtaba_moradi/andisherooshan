import React, { useEffect, useState, Fragment } from "react";
import { get, put, deletes } from "../../../api";
import BoxLoading from "react-loadingg/lib/BoxLoading";
import ShowCardInformation from "../../../components/ShowCardInformation";
import SeminarDetails from "./SeminarDetails";
import card from "../../../util/consts/card";
import ModalBox from "../../../util/modals/ModalBox";
import ModalTrueFalse from "../../../util/modals/ModalTrueFalse";
import Pageination from "../../../components/UI/Pagination";
import SpinnerRotate from "../../../util/Loadings/SpinnerRotate";

const SeminarScreen = () => {
  const [Seminars, setSeminars] = useState();
  const [loading, setLoading] = useState(true);
  const [acceptedIndex, setAcceptedIndex] = useState();
  const [state, setState] = useState();
  const [ModalInpts, setModalInpts] = useState({
    show: false,
    type: false,
  });
  let index = 0;
  useEffect(() => {
    _handelShowSeminar(acceptedIndex);
  }, [state]);
  useEffect(() => {
    apiSeminars(1);
  }, []);
  const apiSeminars = async (page) => await get.seminars(setSeminars, page ? page : "1", setLoading);

  const refreshComponent = async () => {
    if (await apiSeminars(Seminars.page)) {
      setState(index++);
    }
  };
  const _handelShowSeminar = (index) => {
    setAcceptedIndex(index);
  };
  const _handelback = () => {
    setAcceptedIndex();
    apiSeminars(Seminars.page);
  };
  const sendNewValData = async (param) => {
    let Info = await put.seminar(param);
    if (Info) refreshComponent();
  };

  const optionClick = async (event) => {
    switch (event.mission) {
      case "remove":
        removeHandel(event._id);
        break;
      default:
        break;
    }
  };

  // =========================== modal
  // ============================= remove
  const __returnPrevstep = async (value) => {
    onHideModal();
    if (value) if (await deletes.seminar(state.remove.id)) refreshComponent();

    setState({ ...state, remove: { index: "", name: "" } });
  };
  const removeHandel = (id, name) => {
    onShowlModal(true);
    setState({ ...state, remove: { id, name } });
  };

  // =========================== End remove  ====================
  const onHideModal = () => {
    setModalInpts({ ...ModalInpts, show: false });
    refreshComponent();
  };
  const onShowlModal = (type) => {
    if (type) setModalInpts({ ...ModalInpts, show: true, type: true });
    else setModalInpts({ ...ModalInpts, show: true, type: false });
  };
  const _handelPage = (value) => {
    if (value) apiSeminars(value);
  };
  const renderModalInputs = (
    <ModalBox onHideModal={onHideModal} showModal={ModalInpts.show}>
      <ModalTrueFalse modalHeadline={"آیا مطمئن به حذف آن هستید !"} modalAcceptTitle={"بله"} modalCanselTitle={"خیر"} modalAccept={__returnPrevstep} />
    </ModalBox>
  );

  //console.log({ Seminars, Loading });
  const showDataElement =
    Seminars &&
    Seminars.docs.length > 0 &&
    (acceptedIndex >= 0 ? (
      <SeminarDetails information={Seminars.docs[acceptedIndex]} handelback={_handelback} refreshComponent={refreshComponent} sendNewValData={sendNewValData} />
    ) : (
      <ShowCardInformation data={card.seminar(Seminars.docs)} options={{ remove: true }} onClick={_handelShowSeminar} optionClick={optionClick} submitedTitle="مشاهده جزئیات" />
    ));

  return (
    <Fragment>
      {renderModalInputs}
      <div className="countainer-main">
        {acceptedIndex === undefined && (
          <div className="show-total-data">
            <div>
              {" "}
              {"تعداد کل :"} <span>{Seminars ? Seminars.total : 0}</span>
            </div>
            <div>
              {" "}
              {"تعداد نمایش :"} <span>{Seminars ? Seminars.docs.length : 0}</span>
            </div>
          </div>
        )}
        {showDataElement}
        {Seminars && acceptedIndex === undefined && Seminars.pages >= 2 && <Pageination limited={"3"} pages={Seminars ? Seminars.pages : ""} activePage={Seminars ? Seminars.page : ""} onClick={_handelPage} />}
      </div>
      {loading && (
        <div className="staticStyle bgWhite">
          <SpinnerRotate />
        </div>
      )}
    </Fragment>
  );
};

export default SeminarScreen;
