import React, { useState, useEffect } from "react";
import FormInputSection from "./FormInputSection";
import onChanges from "../../../../../util/onChanges";
import states from "../../../../../util/consts/states";
import { post, put } from "../../../../../api";

const AddSection = props => {
  const { editData, modalShow, refreshComponent, addNewSection, onHideModal } = props;
  const [data, setData] = useState({ ...states.addSeminarSection });
  const [state, setState] = useState({ progressPercentImage: null, remove: { index: "", name: "" } });
  const [Loading, setLoading] = useState(false);
  useEffect(() => {
    if (!modalShow) setData({ ...states.addSeminarSection });
  }, [modalShow]);
  console.log({ data });

  const [checkSubmited, setCheckSubmited] = useState(false);
  // ============================= submited
  const submited = () => setCheckSubmited(!checkSubmited);
  const _onSubmited = async e => {
    e.preventDefault();
    let apiRoute, param;
    const formData = {};
    for (let formElementIdentifier in data.Form) formData[formElementIdentifier] = data.Form[formElementIdentifier].value;
    if (editData) {
      param = Object.assign({ data: formData }, { _id: editData._id });
      apiRoute = put.seminarSection(param);
    } else {
      param = Object.assign(formData, addNewSection);
      apiRoute = post.seminarSection(param);
    }
    if (await apiRoute) {
      submited();
      setData({ ...states.addSeminarSection });
      refreshComponent();
      onHideModal();
    }
    console.log(formData);
  };
  // ========================= End submited =================

  const inputChangedHandler = async event => await onChanges.globalChange({ event, data, setData, state, setState, setLoading, imageType: "sliders" });
  const stateArray = [];
  for (let key in data.Form) stateArray.push({ id: key, config: data.Form[key] });
  useEffect(() => {
    let arrayData = [];

    if (editData)
      for (const key in editData)
        for (let index = 0; index < stateArray.length; index++) if (stateArray[index].id === key) arrayData.push({ name: key, value: editData[key] ? editData[key].toString() : editData[key] ? editData[key] : "" });
    if (arrayData.length > 0) inputChangedHandler(arrayData, false, { value: editData.parentType });
  }, [editData]);
  console.log({ stateArray });

  let form = (
    <FormInputSection
      _onSubmited={_onSubmited}
      stateArray={stateArray}
      data={data}
      state={state}
      setData={setData}
      Loading={Loading}
      setLoading={setLoading}
      inputChangedHandler={inputChangedHandler}
      checkSubmited={checkSubmited}
      editData={editData}
    />
  );
  return (
    <div className="countainer-main centerAll ">
      <div className="form-countainer">
        <div className="form-subtitle">{editData ? "تغییر در قسمت " : "افزودن قسمت جدید"}</div>
        <div className="row-give-information">
          {form}
          <div className="btns-container">
            <button className="btns btns-primary" disabled={!data.formIsValid} onClick={_onSubmited}>
              افزودن{" "}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddSection;
