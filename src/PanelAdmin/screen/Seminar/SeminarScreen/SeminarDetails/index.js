import React, { useState, useEffect, useMemo } from "react";
import "./index.scss";
import SeminarSections from "./SeminarSections";
import { Button, ButtonToolbar } from "react-bootstrap";
import ModalSrc from "../../../../util/modals/ModalSrc";
import ModalBox from "../../../../util/modals/ModalBox";
import { put, post, get, deletes } from "../../../../api";
import DetailsThumbnail from "../../../../components/DetailsComponent/DetailsThumbnail";
import toastify from "../../../../util/toastify";
import DetailsStringTextArea from "../../../../components/DetailsComponent/DetailsStringTextArea";
import DetailsStringInput from "../../../../components/DetailsComponent/DetailsStringInput";
import InputsType from "../../../../components/UI/Inputs/InputsType";
import AddSection from "./AddSection";
import DetailsPrice from "../../../../components/DetailsComponent/DetailsPrice";
import ModalTrueFalse from "../../../../util/modals/ModalTrueFalse";
const SeminarDetails = ({ information, handelback, refreshComponent, sendNewValData }) => {
  const [addNewSection, setAddNewSection] = useState();
  const [state, setState] = useState({ remove: { index: "", name: "" } });
  const [ModalInpts, setModalInpts] = useState({
    show: false,
    files: false,
    data: { src: false, type: false, name: false },
  });
  console.log({ ModalInptsData: ModalInpts.data });

  const [edit, setEdit] = useState(false);
  const [remove, setRemove] = useState(false);
  useEffect(() => {
    setAddNewSection((prev) => ({
      ...prev,
      seminarId: information._id,
      number: information.sections.length + 1,
    }));
  }, []);

  // ================================================== modal src ==============================
  // ============================= remove
  const __returnPrevstep = async (value) => {
    onHideModal();
    if (value) if (await deletes.seminarSection(state.remove.id)) refreshComponent();
    setState({ ...state, remove: { index: "", name: "" } });
  };
  // =========================== End remove  ====================
  const onHideModal = () => {
    setModalInpts({ ...ModalInpts, show: false });
    setTimeout(() => {
      setModalInpts({ ...ModalInpts, show: false, data: { src: false, type: false, name: false }, kindOf: false });
    }, 200);
    setEdit();
  };
  const onShowlModal = (event) => {
    console.log({ event });
    if (event ? event.kindOf === "src" : false) setModalInpts({ ...ModalInpts, show: true, data: { src: event.src, type: event.type, name: event.name }, kindOf: event.kindOf });
    else if (event ? event.kindOf === "add" : false) setModalInpts({ ...ModalInpts, show: true, kindOf: event.kindOf });
    else if (event ? event.kindOf === "edit" : false) setModalInpts({ ...ModalInpts, show: true, kindOf: event.kindOf });
    else if (event ? event.kindOf === "question" : false) setModalInpts({ ...ModalInpts, show: true, kindOf: event.kindOf });
  };
  const renderModalInputs = (
    <div className="bgUnset">
      <ModalBox inputData={""} onHideModal={onHideModal} showModal={ModalInpts.show}>
        {ModalInpts.kindOf === "src" && <ModalSrc played={ModalInpts.data} />}
        {ModalInpts.kindOf === "edit" && <AddSection onHideModal={onHideModal} refreshComponent={refreshComponent} modalShow={ModalInpts.show} editData={edit} addNewSection={addNewSection} />}
        {ModalInpts.kindOf === "add" && <AddSection onHideModal={onHideModal} refreshComponent={refreshComponent} modalShow={ModalInpts.show} editData={edit} addNewSection={addNewSection} />}
        {ModalInpts.kindOf === "question" && <ModalTrueFalse modalHeadline={"آیا مطمئن به حذف آن هستید !"} modalAcceptTitle={"بله"} modalCanselTitle={"خیر"} modalAccept={__returnPrevstep} />}
      </ModalBox>
    </div>
  );
  const handelEdit = (play) => {
    setEdit(play);
    onShowlModal({ kindOf: "edit" });
  };
  const handelRemove = (play) => {
    setState({ ...state, remove: { id: play._id } });
    onShowlModal({ kindOf: "question" });
    setEdit();
  };
  // =================================================End modalInputs ======================
  console.log({ information });
  const sendNewVal = async (data) => {
    console.log({ data });
    let param;
    param = Object.assign({ data }, { _id: information._id });
    sendNewValData(param);
  };
  // =================================================End modalInputs ======================
  const elements = (
    <React.Fragment>
      {renderModalInputs}
      <div className="show-card-elements-details opacity-Fade-in-adn-slide-top ">
        <ButtonToolbar>
          <Button onClick={handelback} variant="outline-primary">
            بازگشت <span>{">>>"}</span>
          </Button>
        </ButtonToolbar>
        <DetailsThumbnail toastify={toastify} label={"عکس"} elementType={"inputFile"} imageType={"cover"} fieldName={"cover"} cover={information.cover} sendNewVal={sendNewVal} />
        <div className="detailsRow-wrapper">
          <div>
            {" "}
            <DetailsStringInput toastify={toastify} sendNewVal={sendNewVal} Info={information.title} fieldName={"title"} label={"عنوان"} />
            <DetailsStringTextArea toastify={toastify} sendNewVal={sendNewVal} Info={information.description} Id={information._id} fieldName={"description"} label={"توضیحات"} />
            <DetailsPrice toastify={toastify} sendNewVal={sendNewVal} Info={information.price} Id={information._id} fieldName={"price"} label={"قیمت"} />
          </div>
          <div>
            <DetailsStringTextArea toastify={toastify} sendNewVal={sendNewVal} Info={information.aboutSeminar} Id={information._id} fieldName={"aboutSeminar"} label={"درباره سمینار"} />
          </div>
        </div>
        <SeminarSections onShowModalInputs={onShowlModal} sections={information.sections} handelEdit={handelEdit} handelRemove={handelRemove} />
      </div>
    </React.Fragment>
  );
  return elements;
};

export default SeminarDetails;
