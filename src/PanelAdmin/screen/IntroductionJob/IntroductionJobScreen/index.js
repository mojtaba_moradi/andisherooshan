import React, { useEffect, useState, Fragment } from "react";
import { get, put, deletes } from "../../../api";
import BoxLoading from "react-loadingg/lib/BoxLoading";
import ShowCardInformation from "../../../components/ShowCardInformation";
import IntroductionJobDetails from "./IntroductionJobDetails";
import card from "../../../util/consts/card";
import ModalBox from "../../../util/modals/ModalBox";
import ModalTrueFalse from "../../../util/modals/ModalTrueFalse";
import Pageination from "../../../components/UI/Pagination";
import SpinnerRotate from "../../../util/Loadings/SpinnerRotate";

const IntroductionJobScreen = () => {
  const [IntroductionJobS, setIntroductionJobs] = useState();
  const [loading, setLoading] = useState(true);
  const [acceptedIndex, setAcceptedIndex] = useState();
  const [state, setState] = useState();
  const [editData, setEditData] = useState(false);
  const [ModalInpts, setModalInpts] = useState({
    show: false,
    type: false,
  });
  let index = 0;
  useEffect(() => {
    _handelShowIntroductionJob(acceptedIndex);
  }, [state]);
  useEffect(() => {
    apiIntroductionJob(1);
  }, []);
  const apiIntroductionJob = async (page) => await get.introductionJob(setIntroductionJobs, page ? page : "1", setLoading);

  const refreshComponent = async () => {
    if (await apiIntroductionJob(IntroductionJobS.page)) {
      setState(index++);
    }
  };
  const _handelShowIntroductionJob = (index) => {
    setAcceptedIndex(index);
  };
  const _handelback = () => {
    setAcceptedIndex();
    apiIntroductionJob(IntroductionJobS.page);
  };
  const sendNewValData = async (param) => {
    let Info = await put.introductionJob(param);
    if (Info) refreshComponent();
  };

  const optionClick = async (event) => {
    switch (event.mission) {
      case "remove":
        removeHandel(event._id);
        break;
      default:
        break;
    }
  };
  // =========================== modal
  // ============================= remove
  const __returnPrevstep = async (value) => {
    onHideModal();
    if (value) if (await deletes.introductionJob(state.remove.id)) refreshComponent();

    setState({ ...state, remove: { index: "", name: "" } });
  };
  const removeHandel = (id, name) => {
    onShowlModal(true);
    setState({ ...state, remove: { id, name } });
  };

  // =========================== End remove  ====================
  const onHideModal = () => {
    setModalInpts({ ...ModalInpts, show: false });
    refreshComponent();
    setEditData();
  };
  const onShowlModal = (type) => {
    if (type) setModalInpts({ ...ModalInpts, show: true, type: true });
    else setModalInpts({ ...ModalInpts, show: true, type: false });
  };
  const renderModalInputs = (
    <ModalBox onHideModal={onHideModal} showModal={ModalInpts.show}>
      <ModalTrueFalse modalHeadline={"آیا مطمئن به حذف آن هستید !"} modalAcceptTitle={"بله"} modalCanselTitle={"خیر"} modalAccept={__returnPrevstep} />
    </ModalBox>
  );
  const _handelPage = (value) => {
    if (value) apiIntroductionJob(value);
  };
  //console.log({ IntroductionJobS, Loading });
  const showDataElement =
    IntroductionJobS &&
    IntroductionJobS.docs.length > 0 &&
    (acceptedIndex >= 0 ? (
      <IntroductionJobDetails information={IntroductionJobS.docs[acceptedIndex]} handelback={_handelback} refreshComponent={refreshComponent} sendNewValData={sendNewValData} />
    ) : (
      <ShowCardInformation data={card.introductionJob(IntroductionJobS.docs)} options={{ remove: true }} onClick={_handelShowIntroductionJob} optionClick={optionClick} submitedTitle="مشاهده جزئیات" />
    ));

  return (
    <Fragment>
      {renderModalInputs}
      <div className="countainer-main">
        {acceptedIndex === undefined && (
          <div className="show-total-data">
            <div>
              {" "}
              {"تعداد کل :"} <span>{IntroductionJobS ? IntroductionJobS.total : 0}</span>
            </div>
            <div>
              {" "}
              {"تعداد نمایش :"} <span>{IntroductionJobS ? IntroductionJobS.docs.length : 0}</span>
            </div>
          </div>
        )}
        {showDataElement}
        {IntroductionJobS && acceptedIndex === undefined && IntroductionJobS.pages >= 2 && (
          <Pageination limited={"3"} pages={IntroductionJobS ? IntroductionJobS.pages : ""} activePage={IntroductionJobS ? IntroductionJobS.page : ""} onClick={_handelPage} />
        )}
      </div>
      {loading && (
        <div className="staticStyle bgWhite">
          <SpinnerRotate />
        </div>
      )}
    </Fragment>
  );
};

export default IntroductionJobScreen;
