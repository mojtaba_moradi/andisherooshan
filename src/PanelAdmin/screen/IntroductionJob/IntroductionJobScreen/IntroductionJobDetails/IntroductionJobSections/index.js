import React, { useState, useEffect, Fragment } from "react";
import "./index.scss";
import IntroductionJobPlayBox from "./IntroductionJobPlayBox";
import Strings from "../../../../../value/PanelString/Strings";
import IsNull from "../../../../../components/IsNull";
import PlayBox from "../../../../../components/PlayBox";
const IntroductionJobSections = ({ sections, onShowModalInputs, handelEdit, handelRemove }) => {
  const [state, setState] = useState({
    video: [],
  });
  useEffect(() => {
    setState((prev) => ({ ...prev, video: sections }));
  }, [sections]);

  const elements = (
    <Fragment>
      <div className="card-details-row">
        <div className="courses-title">
          <span>{"قسمت ها"}</span>
          <div className="add-new-Course">
            <button type="button" className="btn btn-primary" onClick={() => onShowModalInputs({ kindOf: "add" })}>
              افزودن
            </button>{" "}
          </div>
        </div>
        <div className="show-courses-container">
          <div className="show-courses">
            {state.video.length ? (
              <PlayBox playSrc={(event) => onShowModalInputs({ ...event, kindOf: "src" })} handelEdit={handelEdit} data={state.video} name={"VIDEO"} handelRemove={handelRemove} />
            ) : (
              <IsNull title={"خالی می باشد"} />
            )}
          </div>
        </div>
      </div>
    </Fragment>
  );
  return <Fragment>{elements}</Fragment>;
};

export default IntroductionJobSections;
