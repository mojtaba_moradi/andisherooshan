import React, { useEffect, useState, useMemo } from "react";
import "./index.scss";
import { get, deletes, put } from "../../../api";
import ShowCardInformation from "../../../components/ShowCardInformation";
import CourseDetails from "./CourseDetails";
import { BoxLoading } from "react-loadingg";
import "react-toastify/dist/ReactToastify.css";
import card from "../../../util/consts/card";
import ModalBox from "../../../util/modals/ModalBox";
import ModalTrueFalse from "../../../util/modals/ModalTrueFalse";
import Pageination from "../../../components/UI/Pagination";
import SpinnerRotate from "../../../util/Loadings/SpinnerRotate";
const CourseScreen = () => {
  const [courses, setCourse] = useState();
  const [loading, setLoading] = useState(true);
  const [acceptedIndex, setAcceptedIndex] = useState();
  const [state, setState] = useState();
  console.log({ acceptedIndex });
  const [editData, setEditData] = useState(false);
  const [ModalInpts, setModalInpts] = useState({
    show: false,
    type: false,
  });
  let index = 0;
  useEffect(() => {
    _handelShowSections(acceptedIndex);
  }, [state]);
  useEffect(() => {
    apiCourses(1);
  }, []);
  const apiCourses = async (page) => await get.courses(setLoading, page ? page : "1", setCourse);

  const _handelShowSections = (index) => {
    setAcceptedIndex(index);
  };
  const _handelback = () => {
    setAcceptedIndex();
    apiCourses(courses.page);
  };
  const refreshComponent = async () => {
    if (await apiCourses(courses.page)) {
      setState(index++);
    }
  };
  const sendNewValData = async (param) => {
    let Info = await put.editCourse(param);
    if (Info) refreshComponent();
  };
  const optionClick = async (event) => {
    switch (event.mission) {
      case "remove":
        removeHandel(event._id);
        break;
      default:
        break;
    }
  };
  // =========================== modal
  // ============================= remove
  const __returnPrevstep = async (value) => {
    onHideModal();
    if (value) if (await deletes.course(state.remove.id)) refreshComponent();
    setState({ ...state, remove: { index: "", name: "" } });
  };
  const removeHandel = (id, name) => {
    onShowlModal(true);
    setState({ ...state, remove: { id, name } });
  };
  // =========================== End remove  ====================
  const onHideModal = () => {
    setModalInpts({ ...ModalInpts, show: false });
    refreshComponent();
    setEditData();
  };
  const onShowlModal = (type) => {
    if (type) setModalInpts({ ...ModalInpts, show: true, type: true });
    else setModalInpts({ ...ModalInpts, show: true, type: false });
  };
  const renderModalInputs = (
    <ModalBox onHideModal={onHideModal} showModal={ModalInpts.show}>
      <ModalTrueFalse modalHeadline={"آیا مطمئن به حذف آن هستید !"} modalAcceptTitle={"بله"} modalCanselTitle={"خیر"} modalAccept={__returnPrevstep} />
    </ModalBox>
  );
  const _handelPage = (value) => {
    if (value) apiCourses(value);
  };
  console.log({ courses, acceptedIndex });
  const showDataElement =
    courses &&
    courses.docs.length > 0 &&
    (acceptedIndex >= 0 ? (
      <CourseDetails information={courses.docs[acceptedIndex]} handelback={_handelback} refreshComponent={refreshComponent} sendNewValData={sendNewValData} />
    ) : (
      <ShowCardInformation data={courses && card.course(courses.docs)} options={{ remove: true }} onClick={_handelShowSections} optionClick={optionClick} submitedTitle={"مشاهده قسمت ها"} />
    ));

  return (
    <React.Fragment>
      {renderModalInputs}
      <div className="countainer-main">
        {acceptedIndex === undefined && (
          <div className="show-total-data">
            <div>
              {" "}
              {"تعداد کل :"} <span>{courses ? courses.total : 0}</span>
            </div>
            <div>
              {" "}
              {"تعداد نمایش :"} <span>{courses ? courses.docs.length : 0}</span>
            </div>
          </div>
        )}
        {showDataElement}
        {courses && acceptedIndex === undefined && courses.pages >= 2 && <Pageination limited={"3"} pages={courses ? courses.pages : ""} activePage={courses ? courses.page : ""} onClick={_handelPage} />}
      </div>
      {loading && (
        <div className="staticStyle bgWhite">
          <SpinnerRotate />
        </div>
      )}
    </React.Fragment>
  );
};

export default CourseScreen;
