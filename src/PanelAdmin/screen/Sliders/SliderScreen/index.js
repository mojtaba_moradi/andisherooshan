import React, { useEffect, useState, Fragment } from "react";
import { get, deletes } from "../../../api";
import BoxLoading from "react-loadingg/lib/BoxLoading";
import ImageCard from "../../../components/ImageCard";
import imageCard from "../../../util/consts/imageCard";
import AddSlider from "../AddSlider";
import ModalBox from "../../../util/modals/ModalBox";
import ModalTrueFalse from "../../../util/modals/ModalTrueFalse";
import SpinnerRotate from "../../../util/Loadings/SpinnerRotate";
const SliderScreen = () => {
  const [Sliders, setSliders] = useState([]);
  const [loading, setLoading] = useState(true);
  const [state, setState] = useState();
  const [editData, setEditData] = useState(false);
  const [sliderId, setSliderId] = useState("");
  const [ModalInpts, setModalInpts] = useState({
    show: false,
    type: false,
  });
  useEffect(() => {
    apiSlide();
  }, []);

  const apiSlide = async () => {
    return await get.sliders(setSliders, setLoading);
  };
  const refreshComponent = async () => await apiSlide();

  // =========================== modal
  const onHideModal = () => {
    setModalInpts({ ...ModalInpts, show: false });
    refreshComponent();
    setEditData();
  };
  const onShowlModal = (type) => {
    if (type) setModalInpts({ ...ModalInpts, show: true, type: true });
    else setModalInpts({ ...ModalInpts, show: true, type: false });
  };
  // ============================= remove
  const __returnPrevstep = async (value) => {
    onHideModal();
    if (value) if (await deletes.slider(state.remove.id)) refreshComponent();
    setState({ ...state, remove: { index: "", name: "" } });
  };
  const removeHandel = (id, name) => {
    onShowlModal(true);
    setState({ ...state, remove: { id, name } });
  };
  // =========================== End remove  ====================
  const renderModalInputs = (
    <div className="bgUnset">
      <ModalBox onHideModal={onHideModal} showModal={ModalInpts.show}>
        {ModalInpts.type ? <ModalTrueFalse modalHeadline={"آیا مطمئن به حذف آن هستید !"} modalAcceptTitle={"بله"} modalCanselTitle={"خیر"} modalAccept={__returnPrevstep} /> : <AddSlider sliderId={sliderId} editData={{ ...editData }} modalShow={ModalInpts.show} onHideModal={onHideModal} />}
      </ModalBox>
    </div>
  );

  // =========================== End modal =====================

  const sendNewValData = async (param) => {
    // let Info = await patch.slider(param);
    // if (Info) refreshComponent();
  };
  const optionClick = async (event) => {
    switch (event.mission) {
      case "remove":
        onShowlModal(true);
        removeHandel(event._id);
        // if (await deletes.Banner(event._id)) refreshComponent();
        break;
      case "block":
        // let data = { fieldChange: "isActive", newValue: event.value.toString() };
        // let param = Object.assign({ data }, { _id: event._id });
        // sendNewValData(param);
        break;
      case "edit":
        setEditData(Sliders[event.index]);
        setSliderId(event._id);
        onShowlModal();
        break;
      default:
        break;
    }
  };
  console.log({ Sliders });
  const showDataElement = (
    <div className="show-card-elements">
      {imageCard.slider(Sliders).map((slider, index) => {
        return <ImageCard options={{ remove: true, edit: true }} optionClick={optionClick} sendNewValData={sendNewValData} key={index} index={index} data={slider} />;
      })}
    </div>
  );

  return (
    <Fragment>
      {renderModalInputs}
      <div className="countainer-main">{showDataElement}</div>;
      {loading && (
        <div className="staticStyle bgWhite">
          <SpinnerRotate />
        </div>
      )}
    </Fragment>
  );
};

export default SliderScreen;
