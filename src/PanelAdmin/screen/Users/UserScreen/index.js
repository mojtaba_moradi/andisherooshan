import React, { useEffect, useState } from "react";
import "./index.scss";
import { get, deletes, put } from "../../../api";
import ShowCardInformation from "../../../components/ShowCardInformation";
import "react-toastify/dist/ReactToastify.css";
import card from "../../../util/consts/card";
import ModalBox from "../../../util/modals/ModalBox";
import ModalTrueFalse from "../../../util/modals/ModalTrueFalse";
import Pageination from "../../../components/UI/Pagination";
import SpinnerRotate from "../../../util/Loadings/SpinnerRotate";
import UserDetails from "./UserDetails";
const UserScreen = () => {
  const [Users, setUsers] = useState();
  const [loading, setLoading] = useState(true);
  const [state, setState] = useState();
  const [editData, setEditData] = useState(false);
  const [acceptedIndex, setAcceptedIndex] = useState();
  const [ModalInpts, setModalInpts] = useState({
    show: false,
    type: false,
  });
  console.log({ Users });

  let index = 0;
  useEffect(() => {
    _handelShowSections(acceptedIndex);
  }, [state]);
  useEffect(() => {
    apiUsers(1);
  }, []);
  const apiUsers = async (page) => await get.users(setUsers, page ? page : "1", setLoading);

  const _handelShowSections = (index) => {
    setAcceptedIndex(index);
  };
  const _handelback = () => {
    setAcceptedIndex();
    apiUsers(Users.page);
  };
  const refreshComponent = async () => {
    if (await apiUsers(Users.page)) {
      setState(index++);
    }
  };
  const sendNewValData = async (param) => {
    // let Info = await put.editCourse(param);
    // if (Info) refreshComponent();
  };
  const optionClick = async (event) => {
    switch (event.mission) {
      case "remove":
        removeHandel(event._id);
        break;
      default:
        break;
    }
  };
  // =========================== modal
  // ============================= remove
  const __returnPrevstep = async (value) => {
    onHideModal();
    if (value) if (await deletes.user(state.remove.id)) refreshComponent();

    setState({ ...state, remove: { index: "", name: "" } });
  };
  const removeHandel = (id, name) => {
    onShowlModal(true);
    setState({ ...state, remove: { id, name } });
  };

  // =========================== End remove  ====================
  const onHideModal = () => {
    setModalInpts({ ...ModalInpts, show: false });
    refreshComponent();
    setEditData();
  };
  const onShowlModal = (type) => {
    if (type) setModalInpts({ ...ModalInpts, show: true, type: true });
    else setModalInpts({ ...ModalInpts, show: true, type: false });
  };
  const renderModalInputs = (
    <ModalBox onHideModal={onHideModal} showModal={ModalInpts.show}>
      <ModalTrueFalse modalHeadline={"آیا مطمئن به حذف آن هستید !"} modalAcceptTitle={"بله"} modalCanselTitle={"خیر"} modalAccept={__returnPrevstep} />
    </ModalBox>
  );
  // ===========================End modal

  //console.log({ users, acceptedIndex });
  const showDataElement =
    Users &&
    Users.docs.length > 0 &&
    (acceptedIndex >= 0 ? (
      <UserDetails information={Users.docs[acceptedIndex]} handelback={_handelback} refreshComponent={refreshComponent} sendNewValData={sendNewValData} />
    ) : (
      <ShowCardInformation data={Users && card.user(Users.docs)} options={{ remove: true, block: true }} onClick={_handelShowSections} optionClick={optionClick} submitedTitle={"مشاهده جزئیات"} />
    ));
  const _handelPage = (value) => {
    if (value) apiUsers(value);
  };
  return (
    <React.Fragment>
      {renderModalInputs}
      <div className="countainer-main">
        {acceptedIndex === undefined && (
          <div className="show-total-data">
            <div>
              {" "}
              {"تعداد کل :"} <span>{Users ? Users.total : 0}</span>
            </div>
            <div>
              {" "}
              {"تعداد نمایش :"} <span>{Users ? Users.docs.length : 0}</span>
            </div>
          </div>
        )}
        {showDataElement}
        {Users && acceptedIndex === undefined && Users.pages >= 2 && <Pageination limited={"3"} pages={Users ? Users.pages : ""} activePage={Users ? Users.page : ""} onClick={_handelPage} />}
      </div>
      {loading && (
        <div className="staticStyle bgWhite">
          <SpinnerRotate />
        </div>
      )}
    </React.Fragment>
  );
};

export default UserScreen;
