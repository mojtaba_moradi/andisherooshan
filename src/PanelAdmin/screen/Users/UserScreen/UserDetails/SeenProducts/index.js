import React, { useEffect, useState } from "react";
import "./index.scss";
import DropdownBoot from "../../../../../components/UI/Inputs/DropdownBoot";
import IsNull from "../../../../../components/IsNull";
import TabelBasic from "../../../../../components/Tables";
import table from "../../../../../util/consts/table";
const SeenProducts = ({ info }) => {
  console.log({ info });
  const [state, setState] = useState({ name: "", data: [], dataHead: [], allSeenSeminars: null, allListenedAudioBooks: null, allSeenCourses: null });
  useEffect(() => {
    if (info) {
      setState({
        ...state,
        name: "سمینار",
        data: table.seenSeminar(info.allSeenSeminars)[1],
        dataHead: table.seenSeminar(info.allSeenSeminars)[0],
        allSeenSeminars: { data: table.seenSeminar(info.allSeenSeminars)[1], dataHead: table.seenSeminar(info.allSeenSeminars)[0] },
        allListenedAudioBooks: { data: table.seenAudioBook(info.allListenedAudioBooks)[1], dataHead: table.seenAudioBook(info.allListenedAudioBooks)[0] },
        allSeenCourses: { data: table.seenCourse(info.allSeenCourses)[1], dataHead: table.seenCourse(info.allSeenCourses)[0] },
      });
    }
    // console.log({ allSeenSeminars, allListenedAudioBooks, allSeenCourses });
  }, [info]);

  console.log({ state });

  const dropDown = [{ title: "کتاب صوتی" }, { title: "دوره" }, { title: "سمینار" }];
  let dropDownData = [];
  for (const index in dropDown) dropDownData.push({ value: dropDown[index].title, title: dropDown[index].title });
  const accepted = (value) => {
    console.log({ value });
    let data;
    switch (value) {
      case "سمینار":
        data = state.allSeenSeminars;
        break;
      case "کتاب صوتی":
        data = state.allListenedAudioBooks;
        break;
      case "دوره":
        data = state.allSeenCourses;
        break;
      default:
        break;
    }
    setState({ ...state, name: value, ...data });
  };
  const showDataElement = (
    <div className="">
      <div style={{ width: "100%", maxWidth: "unset", marginBottom: "5em" }} className="elemnts-box  boxShadow">
        <div className="subtitle-elements">
          <span className="centerAll"> {"قسمت های مشاهده شده"}</span>
          <div className="btns-container">
            <DropdownBoot dropDownData={dropDownData} accepted={accepted} value={state.name} />
          </div>
        </div>

        {state.data.length ? <TabelBasic tbody={state.data} thead={state.dataHead} /> : <IsNull title={"خالی می باشد"} />}
      </div>
    </div>
  );
  return <div>{info ? showDataElement : ""}</div>;
};

export default SeenProducts;
