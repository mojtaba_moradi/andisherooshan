import React, { useState, useEffect, Fragment } from "react";
import "./index.scss";
import ShowCardInformation from "../../../../../components/ShowCardInformation";
import IsNull from "../../../../../components/IsNull";

const UserProducts = ({ basket, bookmark, handelRemove }) => {
  console.log({ basket });

  // //console.log({ video: state.video, voice: state.voice });
  const elements = (
    <Fragment>
      <div className="card-details-row">
        <div className="courses-title">
          <span> فایل های خریداری شده</span>
          {/* <div className="centerAll">({state.video.length + state.voice.length})</div> */}
        </div>
        <div className="show-courses-container">
          <div className="show-courses">
            {basket.length > 0 &&
              basket.map((baskets, index) => {
                console.log({ baskets });

                return (
                  <Fragment key={index + "m"}>
                    <div className="kind-of-data">
                      <span>{baskets.title}</span> <div className="centerAll">({baskets.data.length})</div>
                    </div>
                    {baskets.data.length > 0 ? <ShowCardInformation data={baskets.card(baskets.data)} submitedTitle={false} /> : <IsNull title={"خالی می باشد"} />}
                  </Fragment>
                );
              })}
          </div>
        </div>
      </div>
      {/* <div className="card-details-row">
        <div className="courses-title">
          <span>علاقه مندی بلاگ</span>
          <div className="centerAll">({state.video.length + state.voice.length})</div>
        </div>
        <div className="show-courses-container">
          <div className="show-courses">{bookmark.length > 0 ? <ShowCardInformation data={bookmark} submitedTitle={false} /> : <IsNull title={"خالی می باشد"} />}</div>
        </div>
      </div> */}
    </Fragment>
  );
  return elements;
};

export default UserProducts;
