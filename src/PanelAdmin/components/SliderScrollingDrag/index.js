import React, { useState, useRef, useEffect } from "react";
import useScrollDrag from "./useScrollDrag";
import "./index.scss";
const SliderScrollingDrag = props => {
  const [scrollDrag] = useScrollDrag();
  console.log({ scrollDrag });

  return (
    <div>
      <ul {...scrollDrag} className="slide-wrapper">
        {props.children}
      </ul>
    </div>
  );
};

export default SliderScrollingDrag;
