import React from "react";
import "./index.scss";
import InputPush from "./InputPush";
import InputFile from "./InputFile";
import SearchDropDown from "./SearchDropDown";
import InputDropDownSearch from "./InputDropDownSearch";
import InputFileArray from "./InputFileArray";

const Inputs = props => {
  const {
    progress,
    elementType,
    elementConfig,
    value,
    changed,
    accepted,
    label,
    invalid,
    shouldValidate,
    touched,
    removeHandel,
    dropDownData,
    defaultInputDesable,
    checkSubmited,
    disabled,
    display,
    searchAccepted,
    setSearchAccepted,
    staticTitle
  } = props;
  let inputElement = null;
  const handleKey = event => {
    const form = event.target.form;
    const index = Array.prototype.indexOf.call(form, event.target);
    let keyCode = event.keyCode;
    let numberAccepted = [8, 13];
    if (numberAccepted.includes(keyCode)) {
      if (keyCode === 13) {
        if (form.elements[index + 1]) return form.elements[index + 1].focus();
      } else if (keyCode === 8)
        if (form.elements[index].value) return form.elements[index].value.length - 1;
        else if (form.elements[index - 1]) form.elements[index - 1].focus();
      event.preventDefault();
    }
  };
  const inputClasses = ["InputElement"];

  if (invalid && shouldValidate && touched) {
    inputClasses.push("Invalid");
  }

  switch (elementType) {
    case "input":
      inputElement = <input onKeyDown={handleKey} disabled={disabled} className={inputClasses.join(" ")} {...elementConfig} value={value} onChange={changed} />;
      break;
    case "inputPush":
      inputElement = <InputPush disabled={disabled} className={inputClasses.join(" ")} accepted={accepted} value={value} elementConfig={elementConfig} removeHandel={removeHandel} />;
      break;

    case "inputFile":
      inputElement = <InputFile disabled={disabled} progress={progress} className={inputClasses.join(" ")} onChange={changed} value={value} {...elementConfig} inputLabel={"انتخاب"} label={label} />;
      break;
    case "InputFileArray":
      inputElement = (
        <InputFileArray
          onKeyDown={handleKey}
          removeHandel={removeHandel}
          disabled={disabled}
          progress={progress}
          className={inputClasses.join(" ")}
          onChange={changed}
          value={value}
          {...elementConfig}
          inputLabel={"انتخاب"}
          label={label}
        />
      );
      break;

    case "inputSearch":
      inputElement = (
        <SearchDropDown
          onKeyDown={handleKey}
          checkSubmited={checkSubmited}
          dropDownData={dropDownData}
          accepted={accepted}
          className={inputClasses.join(" ")}
          onChange={changed}
          label={label}
          elementConfig={elementConfig}
          disabled={disabled}
          searchAccepted={searchAccepted}
          setSearchAccepted={setSearchAccepted}
          staticTitle={staticTitle}
        />
      );
      break;
    case "inputDropDownSearch":
      inputElement = (
        <InputDropDownSearch dropDownData={dropDownData} disabled={disabled} className={inputClasses.join(" ")} accepted={accepted} value={value} elementConfig={elementConfig} checkSubmited={checkSubmited} />
      );
      break;
    case "textarea":
      inputElement = <textarea onKeyDown={handleKey} disabled={disabled} className={inputClasses.join(" ")} {...elementConfig} value={value} onChange={changed} />;
      break;
    case "select":
      inputElement = (
        <select disabled={disabled} className={inputClasses.join(" ")} value={value} onChange={changed}>
          {elementConfig.options.map(option => (
            <option key={option.value} value={option.value}>
              {option.displayValue}
            </option>
          ))}
        </select>
      );
      break;
    default:
      return defaultInputDesable ? "" : (inputElement = <input onKeyDown={handleKey} className={inputClasses.join(" ")} {...elementConfig} value={value} onChange={changed} />);
  }

  return (
    <div className={"Input"} style={{ display }}>
      <label className={"Label"}>{label}</label>
      {inputElement}
    </div>
  );
};

export default Inputs;
