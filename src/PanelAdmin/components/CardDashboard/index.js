import React from "react";
import "./index.scss";
const CardDashboard = (props) => {
  const { style, icon, titleTop, titleBottom } = props;
  return (
    <div style={style} className="cardDashboard-wrapper flex">
      <div className={"card-dashboard-row "}>
        <i className={icon} />
        <span className="marginSide1">{titleBottom}</span>
      </div>
      <div className={"card-dashboard-row  "}>
        <div className="textEnd">
          {" "}
          <span>
            {titleTop ? titleTop : "0"}
            {" تومان "}
          </span>
        </div>
      </div>
    </div>
  );
};

export default CardDashboard;
