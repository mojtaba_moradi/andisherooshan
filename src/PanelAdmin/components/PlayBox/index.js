import React from "react";
import "./index.scss";

const PlayBox = ({ data, name, playSrc, handelEdit, handelRemove, disabledEdit, disabledRemove }) => {
  return (
    <div className="courses-subTitle">
      <div className="kind-of-data">
        <span>{name === "VIDEO" ? "تصویری" : "صوتی"}</span>
      </div>
      <div className="courses-box">
        {data.map((play, index) => {
          return (
            <div key={index} className="courses-container">
              <div className="icon-options ">
                {!disabledEdit && (
                  <div onClick={() => handelEdit(play)} className="edited fade-in-up">
                    <i className="icon-pencil transition0-2 rotate-84"></i>
                  </div>
                )}
                {!disabledRemove && (
                  <div onClick={() => handelRemove(play)} className="removed fade-in-up">
                    <i className="icon-cancel"></i>
                  </div>
                )}
              </div>

              <div className="courses-icons">
                <i className={name === "VIDEO" ? "icon-video" : " icon-music"} />
              </div>
              <div className="course-text">
                <span className="course-title">{play.title}</span>
                <span className="course-description" title={play.description}>
                  {play.description}
                </span>
              </div>
              <div onClick={() => playSrc(name === "VIDEO" ? { src: play.video, name } : { src: play.voice, name }, name)} className="btn-play-icon transition0-2">
                <i className="icon-play"></i>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default PlayBox;
