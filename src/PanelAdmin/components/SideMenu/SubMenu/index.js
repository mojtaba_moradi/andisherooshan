import React from "react";
import { Link } from "react-router-dom";

const SubMenu = ({ menu, setMenuTitle, showLi, selectedMenu }) => {
  const location = window.location.href;

  const onSubMenuClicked = (subMenu, menu) => {
    setMenuTitle(menu.menuTitle);
  };

  return (
    <ul
      style={{
        height: showLi === menu.menuTitle ? menu.subMenu.length * 3 + "rem" : ""
      }}
      className={`side-child-navigation transition0-3 ${showLi === menu.menuTitle && menu.subMenu.length ? "showIn" : "showOut"}`}
    >
      {menu.subMenu.map((child, i) => (
        <li className={`side-iteme`} key={i} onClick={() => onSubMenuClicked(child, menu)}>
          <Link
            className={`side-link side-child-ling ${child.route === selectedMenu || location.includes(child.route) ? "activedSideChild" : ""} `}
            id="sideChildTitle"
            to={child.route}
          >
            {child.title}
          </Link>
        </li>
      ))}
    </ul>
  );
};

export default SubMenu;
