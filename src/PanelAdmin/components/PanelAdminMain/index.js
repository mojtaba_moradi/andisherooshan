import React from "react";
import "./index.scss";
import { Switch, Route, Redirect } from "react-router-dom";
import routes from "../../value/routes";
import ProtectedRoute from "../../util/ProtectedRoute";
import formatRoute from "../../util/formatRoute";

const PanelAdminMain = () => {
  return (
    <main className="panelAdmin-main-container">
      <Switch>
        {formatRoute.map((route, index) => {
          return <ProtectedRoute key={index + "m"} path={route.path} component={route.component} />;
        })}
        <Redirect from={routes.GS_ADMIN_LOGIN} to={routes.GS_ADMIN_DASHBOARD} />

        <Redirect exact from={routes.GS_PANEL_ADMIN_TITLE} to={routes.GS_ADMIN_DASHBOARD} />
      </Switch>
    </main>
  );
};

export default PanelAdminMain;
