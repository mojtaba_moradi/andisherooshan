import React, { useState, useEffect, useMemo } from "react";
import "./index.scss";
import BlogDetailsImg from "./BlogDetailsImg";
import BlogDetailsAbout from "./BlogDetailsAbout";
import BlogSections from "./BlogSections";
import { Button, ButtonToolbar } from "react-bootstrap";
import ModalSrc from "../../../../util/modals/ModalSrc";
import ModalBox from "../../../../util/modals/ModalBox";
import AddSectionInputs from "../../../../util/input/inputsMind/AddSectionInputs";
import BlogDetailsTitle from "./BlogDetailsTitle";
import InputsType from "../../../../util/input/inputsMind/InputsType";
import { put, post } from "../../../../api";
import BlogDetailsDescription from "./BlogDetailsDescription";
import BlogDetailsCategory from "./BlogDetailsCategory";
const BlogDetails = ({ information, handelback, refreshComponent }) => {
  const [addBlog, setAddBlog] = useState();
  const [edit, setEdit] = useState(false);
  const [ModalInpts, setModalInpts] = useState({
    show: false,
    files: false
  });
  let activeInputData = false;
  if (ModalInpts.files === "cover") activeInputData = { valid: true, name: "cover", value: { label: "تغییر عکس دوره", type: "file" } };
  else activeInputData = { valid: information.type, name: "dataUrl", value: { label: "آپلود فایل جدید", type: "file" } };

  const [ModalSource, setModalSource] = useState({
    showModalSource: false,
    Blog: { src: null, type: null, name: null }
  });

  const modalReturnData = async upload => {
    let param;
    let name = ModalInpts.files;
    console.log({ upload, name });

    if (upload && name) {
      const data = { fieldChange: name, newValue: Object.values(upload)[0] };
      param = Object.assign({ data }, { _id: information._id });
      console.log({ paramsssss: param });

      let BlogImage = await put.editBlog(param);
      if (BlogImage) refreshComponent();
    }
  };

  // ================================================== modal src ==============================
  const onHideModalSource = () => {
    setModalSource({
      ...ModalSource,
      showModalSource: false,
      Blog: { src: null, type: null }
    });
  };
  const onShowModalSource = (src, name, type) => {
    //console.log({ src, name, type });

    setModalSource({
      ...ModalSource,
      showModalSource: true,
      Blog: { src, type, name }
    });
  };
  //console.log({ ModalSource });

  const renderModalSources = <ModalSrc showModalSource={ModalSource.showModalSource} onHideModalSource={onHideModalSource} played={ModalSource.Blog} />;
  // =================================================End modal src =================

  // ================================================= modalInputs ======================

  const onHideModal = () => {
    setModalInpts({ ...ModalInpts, show: false });
    setTimeout(() => {
      setModalInpts({ ...ModalInpts, show: false });
    }, 200);
  };
  const onShowlModal = files => {
    console.log({ files });

    if (files === "cover" || files === "dataUrl") setModalInpts({ ...ModalInpts, show: true, files });
    else setModalInpts({ ...ModalInpts, show: true, files: "" });
  };
  // console.log({ ModalInpts });

  const renderModalInputs = (
    <ModalBox onHideModal={onHideModal} showModal={ModalInpts.show}>
      <InputsType entry={activeInputData} onHideModal={onHideModal} inputsData={modalReturnData} showModal={ModalInpts.show} acceptedTitle="ثبت" />
    </ModalBox>
  );
  // console.log({ information });

  // =================================================End modalInputs ======================
  const elements = (
    <React.Fragment>
      {renderModalInputs} {renderModalSources}
      <div className="show-card-elements-details opacity-Fade-in-adn-slide-top ">
        <ButtonToolbar>
          <Button onClick={handelback} variant="outline-primary">
            بازگشت <span>{">>>"}</span>
          </Button>
        </ButtonToolbar>
        <BlogDetailsImg modalEditCover={() => onShowlModal("cover")} cover={information.cover} />
        <BlogDetailsTitle BlogTitle={information.title} BlogId={information._id} refreshComponent={refreshComponent} />
        <BlogDetailsDescription BlogDescription={information.description} BlogId={information._id} refreshComponent={refreshComponent} />
        <BlogDetailsAbout aboutBlog={information.aboutBlog} BlogId={information._id} refreshComponent={refreshComponent} />
        <BlogDetailsCategory categoryBlog={information.category} BlogId={information._id} refreshComponent={refreshComponent} />
        <BlogSections onShowModalInputs={onShowlModal} blog={information} onShowModalSource={onShowModalSource} handelEdit={() => onShowlModal("dataUrl")} />
      </div>
    </React.Fragment>
  );

  return elements;
};

export default BlogDetails;
